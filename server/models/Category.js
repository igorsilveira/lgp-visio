var keystone = require('keystone')
var Types = keystone.Field.Types

var Category = new keystone.List('Category', {
  autokey: { path: 'key', from: 'name', unique: true },
  track: true,
})

Category.add({
  name: { type: String, required: true, index: true },
  icon: {
    type: Types.CloudinaryImage,
    folder: 'visiokids/',
    autoCleanup: true,
  },
  color: {
    type: Types.Select,
    options: [
      { value: '#F5AA3B', label: 'Yellow' },
      { value: '#666666', label: 'Grey' },
      { value: '#00A397', label: 'Green' },
      { value: '#0071B6', label: 'Blue' },
      { value: '#DD3333', label: 'Red' },
    ],
    emptyOption: false,
  },
})

// Relationship definitions are optional
Category.relationship({
  ref: 'Thread',
  refPath: 'categories',
  path: 'categoryThreads',
})

Category.relationship({ ref: 'Experience', refPath: 'categories' })

Category.defaultColumns = 'name'
Category.track = true
Category.register()

module.exports = Category
