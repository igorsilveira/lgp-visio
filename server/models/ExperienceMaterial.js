var keystone = require('keystone')
var Types = keystone.Field.Types

/**
 * Step Model
 * ==========
 */
// Create a new Keystone list called Step
var ExperienceMaterial = new keystone.List('ExperienceMaterial', {
  label: 'Materiais na Experiencia',
  autokey: {
    from: 'name createdAt',
    path: 'key',
    unique: true,
  },
  defaultSort: '-createdAt',
})

ExperienceMaterial.add({
  name: {
    type: String,
    initial: true,
    index: true,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  experience: {
    type: Types.Relationship,
    ref: 'Experience',
    many: false,
    noedit: true,
  },
  material: {
    type: Types.Relationship,
    ref: 'Material',
    initial: true,
    many: false,
    noedit: false,
    required: true,
  },
  quantity: {
    type: Types.Number,
    initial: true,
    required: true,
    deafult: 1,
  },
  type: {
    type: Types.Select,
    options: [
      { value: 'L', label: 'Litros' },
      { value: 'mL', label: 'Mililitros' },
      { value: 'g', label: 'Gramas' },
      { value: 'mg', label: 'Miligramas' },
      { value: 'Kg', label: 'Quilogramas' },
      { value: 'x', label: 'Vezes' },
      { value: '', label: 'None' },
    ],
    default: 'none',
    initial: true,
    required: true,
    deafult: 1,
  },
})

// Setting the default order of the columns on the admin tab
ExperienceMaterial.defaultColumns = 'name'
ExperienceMaterial.relationship({
  path: 'experience',
  ref: 'Experience',
  refPath: 'materiais',
})

ExperienceMaterial.track = {
  createdAt: true,
  createdBy: true,
}

ExperienceMaterial.register()
