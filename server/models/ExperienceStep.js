var keystone = require('keystone')
var Types = keystone.Field.Types

/**
 * Step Model
 * ==========
 */
// Create a new Keystone list called Step
var ExperienceStep = new keystone.List('ExperienceStep', {
  label: 'Steps',
  autokey: {
    from: 'name createdAt',
    path: 'key',
    unique: true,
  },
  defaultSort: '-createdAt',
})

ExperienceStep.add({
  name: {
    type: String,
    initial: true,
    index: true,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  experience: {
    type: Types.Relationship,
    ref: 'Experience',
    many: false,
    noedit: true,
  },
  image: {
    type: Types.CloudinaryImage,
    folder: 'experienceSteps/',
    initial: true,
    autoCleanup: true,
  },
  instructions: {
    type: Types.TextArray,
    initial: true,
    required: true,
  },
})

// Setting the default order of the columns on the admin tab
ExperienceStep.defaultColumns = 'name'
ExperienceStep.relationship({
  path: 'experience',
  ref: 'Experience',
  refPath: 'steps',
})

ExperienceStep.track = {
  createdAt: true,
  createdBy: true,
}

ExperienceStep.register()
