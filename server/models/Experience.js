var keystone = require('keystone')
var Types = keystone.Field.Types

// Create a new Keystone list called Experience
var Experience = new keystone.List('Experience', {
  defaultSort: '-createdAt',
})

var myStorage = new keystone.Storage({
  adapter: keystone.Storage.Adapters.FS,
  fs: {
    path: keystone.expandPath('./server/public/uploads/files/music'), // required; path where the files should be stored
    publicPath: '/server/public/uploads/files/music', // path where files will be served
  },
})

// Finally we are gonna add the fields for our Experience
Experience.add({
  name: {
    type: String,
    required: true,
    initial: true,
    index: true,
  },
  categories: {
    type: Types.Relationship,
    ref: 'Category',
    initial: true,
    many: true,
  },
  description: {
    type: Types.Textarea,
    height: 200,
  },
  difficulty: {
    type: Types.Select,
    options: 'fácil, médio, difícil',
    default: 'fácil',
  },
  reward: {
    type: Number,
    default: 100,
  },
  duration: {
    type: Types.Number,
    note: 'Estimated duration in minutes',
  },
  // materialList: {
  //   height: 150,
  //   type: Types.TextArray,
  // },
  image: {
    type: Types.CloudinaryImage,
    folder: 'experiences/',
    autoCleanup: true,
  },
  steps: {
    type: Types.Relationship,
    ref: 'ExperienceStep',
    createInline: true,
    many: true,
    filters: {
      experience: ':_id'
    },
  },
  materiais: {
    type: Types.Relationship,
    ref: 'ExperienceMaterial',
    createInline: true,
    many: true,
    filters: {
      experience: ':_id'
    },
  },
  assets: {
    type: Types.Relationship,
    ref: 'Asset',
    createInline: true,
    many: true,
  },
  soundDescription: {
    type: Types.File,
    storage: myStorage,
  },
  threads: {
    type: Types.Relationship,
    ref: 'Thread',
    many: true,
    filters: {
      experiencies: ':_id'
    },
  }
},
'Meta Info', {
  state: {
    type: Types.Select,
    options: 'draft, published, archived',
    default: 'draft',
  },
  author: {
    type: Types.Relationship,
    ref: 'User',
  },
  createdAt: {
    type: Types.Date,
    default: Date.now,
  },
  publishedAt: {
    type: Types.Date,
    index: true,
  },
}
)

Experience.schema.pre('remove', function (next, err) {
  // TODO
  keystone
    .list('ExperienceStep')
    .model.remove({
      experience: this._id
    })
    .then(array => {
      next()
    })
    .catch(error => {
      // TODO error handle
      throw error
    })
})

Experience.schema.pre('save', function (next) {
  this.steps.forEach(step => {
    keystone
      .list('ExperienceStep')
      .model.updateOne({
        _id: step
      }, {
        experience: this
      }, function (err, res) {
        console.log(res.n, 'Steps updated')
      })
  })

  next()
})

Experience.schema.pre('remove', function (next, err) {
  // TODO
  keystone
    .list('Asset')
    .model.remove({
      experience: this._id
    })
    .then(array => {
      next()
    })
    .catch(error => {
      // TODO error handle
      throw error
    })
})

Experience.schema.pre('save', function (next) {
  this.assets.forEach(asset => {
    keystone
      .list('Asset')
      .model.updateOne({
        _id: asset
      }, {
        experience: this
      }, function (err, res) {
        console.log(res.n, 'Assets updated')
      })
  })

  // if (this.state === 'published') this.publishedAt
  next()
})

Experience.schema.pre('remove', function (next, err) {
  // TODO
  keystone
    .list('ExperienceMaterial')
    .model.remove({
      experience: this._id
    })
    .then(array => {
      next()
    })
    .catch(error => {
      // TODO error handle
      throw error
    })
})

Experience.schema.pre('save', function (next) {
  this.materiais.forEach(material => {
    keystone
      .list('ExperienceMaterial')
      .model.updateOne({
        _id: material
      }, {
        experience: this
      }, function (err, res) {
        console.log(res.n, 'Materials updated')
      })
  })

  // if (this.state === 'published') this.publishedAt
  next()
})

// Relationship definitions are optional
Experience.relationship({
  ref: 'Thread',
  refPath: 'experiencies',
  path: 'experienceThread',
})

Experience.track = {
  createdAt: true,
  createdBy: true,
}

// Setting the default order of the columns on the admin tab
Experience.defaultColumns = 'name, state|20%, author, publishedAt|15%'
Experience.register()
