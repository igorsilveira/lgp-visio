var keystone = require('keystone')
var User = keystone.list('User')
var Experience = keystone.list('Experience')
var Types = keystone.Field.Types

// Create a new Keystone list called Experience
var UserResult = new keystone.List('UserResult')

// Finally we are gonna add the fields for our Experience
UserResult.add({
  user: {
    type: Types.Relationship,
    ref: 'User',
    many: false,
    required: true,
    initial: true
  },
  resultImage: {
    type: Types.CloudinaryImage,
    folder: 'users/experienceResult/' ,
    autoCleanup: true,
  },
  experience: {
    type: Types.Relationship,
    ref: 'Experience',
    many: false,
    required: true,
    initial: true
  },
})


// Add Score
UserResult.schema.pre('save', function (next) {
  if (!this.isNew)
    return next()
    
  User.model.findById(this.user).exec(function(err, user) {
    if (err) return next()
    if (!user) return next()

    Experience.model.findById(this.experience)
      .populate('categories').exec(function (err, exp) {
        if (err) return next()
        if (!exp) return next()

        let score = user.getScore
        exp.categories.map(category => {
          score[category.key].score += exp.reward
          score[category.key].count++
        })
        score.total.score += exp.reward
        score.total.count++

        user.score = JSON.stringify(score)
        user.executed = user.executed.concat([exp._id])
        user.save()
      
        return next()
      })
  }.bind(this))
    
})


UserResult.schema.pre('remove', function (next) {
  User.model.findById(this.user).exec(function(err, user) {
    if (err) return next()
    if (!user) return next()

    Experience.model.findById(this.experience)
      .populate('categories').exec(function (err, exp) {
        if (err) return next()
        if (!exp) return next()

        let score = user.getScore
        exp.categories.map(category => {
          score[category.key].score -= exp.reward
          score[category.key].count--
        })
        score.total.score -= exp.reward
        score.total.count--

        user.score = JSON.stringify(score)
        let index = user.executed.indexOf(exp._id)
        if (index > -1) {
          user.executed.splice(index, 1)
        }
        user.save()

        return next()
      })
  }.bind(this))
})

// Setting the default order of the columns on the admin tab
UserResult.defaultColumns = 'user, experience'
UserResult.register()
