var keystone = require('keystone')
var Types = keystone.Field.Types

var Asset = new keystone.List('Asset', {
  label: 'Assets',
  autokey: {
    from: 'name createdAt',
    path: 'key',
    unique: true,
  },
})

var myStorage = new keystone.Storage({
  adapter: keystone.Storage.Adapters.FS,
  fs: {
    path: keystone.expandPath('./server/public/uploads/files'), // required; path where the files should be stored
    publicPath: '/server/public/uploads/files', // path where files will be served
  },
})

Asset.add({
  name: {
    type: String,
    initial: true,
    index: true,
    required: true,
  },
  file: {
    type: Types.File,
    storage: myStorage,
    initial: true,
    required: true,
  },
  experience: {
    type: Types.Relationship,
    ref: 'Experience',
    many: false,
    noedit: true,
  },
  category: { type: String }, // Used to categorize widgets.
  createdAt: {
    type: Date,
    default: Date.now,
  },
  url: { type: String },
  fileType: {
    type: String,
    initial: true,
    required: true,
  },

})


Asset.defaultColumns = 'name'
Asset.relationship({
  path: 'experience',
  ref: 'Experience',
  refPath: 'assets',
})
Asset.track = {
  createdAt: true,
  createdBy: true,
}
Asset.register()
