var keystone = require('keystone')
var Types = keystone.Field.Types

// Create a new Keystone list called Experience
var Thread = new keystone.List('Thread', {
  autokey: {
    path: 'key',
    from: 'name',
    unique: true
  },
  defaultSort: '-createdAt',
})

// Finally we are gonna add the fields for our Experience
Thread.add({
  name: {
    type: String,
    required: true,
  },
  state: {
    type: Types.Select,
    options: 'draft, published, archived',
    default: 'draft',
  },
  ranking: {
    type: Number,
    default: 0,
  },
  author: {
    type: Types.Relationship,
    ref: 'User',
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  publishedAt: Date,
  image: {
    type: Types.CloudinaryImage,
    folder: 'thread/',
    autoCleanup: true,
  },
  description: {
    type: Types.Markdown,
    height: 400,
  },
  categories: {
    type: Types.Relationship,
    ref: 'Category',
    many: true,
    initial: true,
    require: true,
  },
  experiencies: {
    type: Types.Relationship,
    ref: 'Experience',
    many: true,
  },
  comments: {
    type: Types.Relationship,
    ref: 'Comment',
    many: true,
    createInline: true,
    sort: 'createdAt',
    filters: {
      thread: ':_id'
    },
  },
})
// Relationship definitions are optional
Thread.relationship({
  ref: 'Comment',
  refPath: 'thread',
  path: 'commentsThreads',
})
// Setting the default order of the columns on the admin tab
Thread.defaultColumns = 'name, state|20%, categories, publishedAt|15%'
Thread.register()
