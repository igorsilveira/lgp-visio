var keystone = require('keystone')
var Types = keystone.Field.Types

/**
 * Step Model
 * ==========
 */
// Create a new Keystone list called Step
var Material = new keystone.List('Material', {
  label: 'Materiais',
  autokey: {
    from: 'name createdAt',
    path: 'key',
    unique: true,
  },
  defaultSort: '-createdAt',
})

Material.add({
  name: {
    type: String,
    initial: true,
    index: true,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  image: {
    type: Types.CloudinaryImage,
    folder: 'materiais/',
    initial: true,
    autoCleanup: true,
  },
})

Material.track = {
  createdAt: true,
  createdBy: true,
}

Material.register()
