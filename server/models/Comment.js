var keystone = require('keystone')
var Types = keystone.Field.Types

var Comment = new keystone.List('Comment', {
  autokey: { path: 'key', from: 'value', unique: true },
  track: true,
})

Comment.add({
  value: { type: String, required: true, index: true, initial: true },
  author: { type: Types.Relationship, ref: 'User', required: true, initial: true },
  approved: { type: Boolean, default: false },
  votes: { type: Number, default: 0 },
  thread: {
    type: Types.Relationship,
    ref: 'Thread',
    many: false,
    required: true,
    initial: true,
    noedit: true,
  },
})

Comment.schema.pre('save', function (next) {
  this.wasNew = this.isNew
  next()
})

Comment.schema.post('save', function () {
  if (this.wasNew) {
    keystone
      .list('Thread')
      .model.updateOne({ _id: this.thread }, { $push: { comments: this._id } }, function (err, res) {
        if (err)
        { console.log(err, 'Thread not updated') }
        if (res.n === 0)
        { console.log(err, 'Thread not updated') }
      })
  }
})

Comment.schema.pre('remove', function (next) {
  keystone
    .list('Thread')
    .model.updateOne({ _id: this.thread }, { $pull: { comments: this._id } }, function (err, res) {
      if (err)
      { console.log(err, 'Thread not updated') }
      if (res.n === 0)
      { console.log(err, 'Thread not updated') }
    })
  next()
})

// Relationship definitions are optional
Comment.relationship({
  ref: 'Thread',
  refPath: 'comments',
  path: 'commentsThreads',
})

Comment.defaultColumns = 'value|80%'
Comment.register()

module.exports = Comment
