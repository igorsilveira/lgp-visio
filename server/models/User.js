var keystone = require('keystone')
var Types = keystone.Field.Types

/**
 * User Model
 * ==========
 */
var User = new keystone.List('User')

User.add(
  {
    method: {
      type: String,
      enum: ['local', 'facebook', 'google'],
      required: true,
      default: 'local',
    },
    name: { type: Types.Name, required: true, index: true },
    email: {
      type: Types.Email,
      initial: true,
      required: true,
      unique: true,
      index: true,
    },
    image: {
      type: Types.CloudinaryImage,
      folder: 'users/',
      autoCleanup: true,
    },
    socialImage: {
      type: Types.Url,
    },
    password: { type: Types.Password, initial: true },
    avatar: {
      type: Types.CloudinaryImage,
      whenExists: 'overwrite',
      folder: 'users/avatars/',
      autoCleanup: true,
    },
    facebookId: {
      type: String,
    },
    googleId: {
      type: String,
    },
    executed: {
      type: Types.Relationship,
      ref: 'Experience',
      many: true,
    },
    score: {
      type: Types.Code,
      lang: 'json',
      default: '{ "ciencia": {"score": 0, "count": 0}, "tecnologia": {"score": 0, "count": 0}, "engenharia": {"score": 0, "count": 0}, "artes": {"score": 0, "count": 0}, "matematica": {"score": 0, "count": 0}, "total": {"score": 0, "count": 0} }',
      noedit: true,
    },
    tutorial: {
      type: Boolean,
      hidden: true
    }
  },
  'Permissions',
  {
    role: {
      type: Types.Select,
      options: 'regular, monitor, editor, admin',
      initial: true,
      required: true,
      default: 'regular',
    },
  }
)

User.schema.virtual('getScore').get(function () {
  return JSON.parse(this.score)
})

// Provide access to Keystone
User.schema.virtual('canAccessKeystone').get(function () {
  return (
    this.role === 'monitor' || this.role === 'editor' || this.role === 'admin'
  )
})

// Relationship definitions are optional
User.relationship({ ref: 'Comment', refPath: 'author', path: 'commentUser' })
User.relationship({ ref: 'Thread', refPath: 'author', path: 'threadUser' })
User.relationship({
  ref: 'Experience',
  refPath: 'author',
  path: 'experienceUser',
})
User.relationship({ref: 'UserResult', refPath:'user', path:'user', label: 'Results upload'})

/**
 * Registration
 */
User.defaultColumns = 'name, email, role'
User.register()
