var keystone = require('keystone')
var Types = keystone.Field.Types

/**
 * Group Model
 * =============
 */

var Group = new keystone.List('Group', {
  noedit: false,
})

Group.add({
  name: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  image: {
    type: Types.CloudinaryImages,
    label: 'Pictures',
    folder: 'groups/',
    autoCleanup: true,

  },
  experience: {
    type: Types.Relationship,
    ref: 'Experience',
    note: 'Experiences this group was a part of',
    many: true,
  },
})

Group.defaultSort = '-createdAt'
Group.defaultColumns = 'name, createdAt|20%'
Group.register()
