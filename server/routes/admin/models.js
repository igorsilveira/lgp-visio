// ADMIN
module.exports.ADMIN_MODELS = [
  'assets',
  'categories',
  'comments',
  'enquiries',
  'experiences',
  'experience-materials',
  'experience-steps',
  'groups',
  'materials',
  'threads',
  'users',
  'user-results',
]

module.exports.ADMIN_NAV = {
  enquiries: ['enquiries'],
  users: ['users', 'user-results'],
  experiences: ['experiences', 'experience-steps', 'experience-materials', 'materials', 'categories', 'assets'],
  groups: ['groups'],
  forum: ['threads', 'comments'],
}

// MONITOR
module.exports.MONITOR_MODELS = [
  'assets',
  'groups',
  'threads',
]

module.exports.MONITOR_NAV = {
  experiences: ['assets'],
  groups: ['groups'],
  forum: ['threads'],
}

// EDITOR
module.exports.EDITOR_MODELS = [
  'assets',
  'experiences',
  'experience-materials',
  'experience-steps',
  'groups',
  'materials',
  'threads',
]

module.exports.EDITOR_NAV = {
  experiences: ['experiences', 'experience-steps', 'experience-materials', 'materials', 'assets'],
  groups: ['groups'],
  forum: ['threads'],
}
