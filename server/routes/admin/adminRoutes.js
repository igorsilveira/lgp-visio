var keystone = require('keystone')
const ADMIN_MODELS = require('./models').ADMIN_MODELS
const ADMIN_NAV = require('./models').ADMIN_NAV
const MONITOR_MODELS = require('./models').MONITOR_MODELS
const MONITOR_NAV = require('./models').MONITOR_NAV
const EDITOR_MODELS = require('./models').EDITOR_MODELS
const EDITOR_NAV = require('./models').EDITOR_NAV

/**
	Prevents people from accessing protected pages when they're not signed in
 */
exports.restrictModels = function (req, res, next) {
  if (req.originalUrl !== '/keystone/')
  { return next() }

  if (req.user != null) {
    var nav = {}
    switch (req.user.role) {
    case 'editor':
      nav = EDITOR_NAV
      break
    case 'monitor':
      nav = MONITOR_NAV
      break
    case 'admin':
      nav = ADMIN_NAV
      break
    default:
      return next()
    }
    hideModels(req, res)
    keystone.set('nav', nav)
  }
  next()
}

function hideModels (req, res, next) {
  if (req.originalUrl !== '/keystone/')
  { return next ? next() : null }

  if (req.user != null) {
    let modelsShow = []
    switch (req.user.role) {
    case 'editor':
      modelsShow = EDITOR_MODELS
      break
    case 'monitor':
      modelsShow = MONITOR_MODELS
      break
    case 'admin':
      modelsShow = ADMIN_MODELS
      break
    default:
      return next ? next() : null
    }

    ADMIN_MODELS.map(model => {
      if (modelsShow.includes(model))
      { keystone.list(model).set('hidden', false) }
      else
      { keystone.list(model).set('hidden', true) }
    })
  }
  if (next)
  { next() }
} exports.hideModels = hideModels

exports.restrictAPIRoutes = function (req, res, next) {
  if (req.method === 'GET')
  { return next() }

  if (req.user != null) {
    switch (req.user.role) {
    case 'editor':
      for (let i = 0; i < ADMIN_MODELS.length; i++) {
        if (!EDITOR_MODELS.includes(ADMIN_MODELS[i])) {
          if (req.originalUrl.includes(ADMIN_MODELS[i])
          || req.originalUrl.includes('delete')) {
            return res.status(403).json({ error: 'Permission Denied' })
          }
        }
      }
      break
    case 'monitor':
      for (let i = 0; i < ADMIN_MODELS.length; i++) {
        if (!MONITOR_MODELS.includes(ADMIN_MODELS[i])) {
          if (req.originalUrl.includes(ADMIN_MODELS[i])
          || req.originalUrl.includes('delete')) {
            return res.status(403).json({ error: 'Permission Denied' })
          }
        }
      }
      break
    default:
      break
    }
  }
  next()
}

exports.restrictRoutes = function (req, res, next) {
  if (req.method !== 'GET')
  { return next() }
  if (!req.originalUrl.includes('/keystone/'))
  { return next() }

  if (req.user != null) {
    switch (req.user.role) {
    case 'editor':
      for (let i = 0; i < ADMIN_MODELS.length; i++) {
        if (!EDITOR_MODELS.includes(ADMIN_MODELS[i])) {
          if (req.originalUrl.includes('/keystone/' + ADMIN_MODELS[i])) {
            return res.redirect('/keystone/')
          }
        }
      }
      break
    case 'monitor':
      for (let i = 0; i < ADMIN_MODELS.length; i++) {
        if (!MONITOR_MODELS.includes(ADMIN_MODELS[i])) {
          if (req.originalUrl.includes('/keystone/' + ADMIN_MODELS[i])) {
            return res.redirect('/keystone/')
          }
        }
      }
      break
    default:
      break
    }
  }
  next()
}
