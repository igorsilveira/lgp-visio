var keystone = require('keystone')
var passport = require('passport')
var LocalStrategy = require('passport-local').Strategy
var FacebookStrategy = require('passport-facebook').Strategy
var GoogleStrategy = require('passport-google-oauth2').Strategy
var User = keystone.list('User')

passport.serializeUser(function (user, cb) {
  cb(null, user._id)
})

passport.deserializeUser(function (id, cb) {
  User.model.findById(id, function (er, user) {
    cb(er, user)
  })
})

passport.use(
  new LocalStrategy({
    usernameField: 'email',
    session: false },
  function (username, password, done) {
    User.model.findOne({ email: username }, function (err, user) {
      if (err) { return done(err) }
      if (!user) { return done(null, false) }
      user._.password.compare(password, function (err, isMatch) {
        if (!err && isMatch) {
          return done(null, user)
        } else {
          return done(null, false)
        }
      })
    })
  }
  ))

passport.use(new FacebookStrategy({
  clientID: process.env.FACEBOOK_CLIENT_ID,
  clientSecret: process.env.FACEBOOK_CLIENT_SECRET,
  callbackURL: '/api/user/auth/facebook/callback',
},
function (accessToken, refreshToken, profile, cb) {
  User.model.findOne({ facebookId: profile.id }, function (err, user) {
    if (!user) {
      var newUser = new User.model()
      var data = {
        'name.first': profile.displayName,
        'facebookId': profile.id,
        'email': profile.emails ? profile.emails[0].value : profile.id + '@facebook.com',
        'method': 'facebook',
      }
      newUser.getUpdateHandler().process(data, function (err) {
        if (err) {
          return cb(err, false)
        }
        return cb(err, newUser)
      })
    } else
      return cb(err, user)
  })
}
))

passport.use(new GoogleStrategy({
  clientID: process.env.GOOGLE_CLIENT_ID,
  clientSecret: process.env.GOOGLE_CLIENT_SECRET,
  callbackURL: '/api/user/auth/google/callback',
  passReqToCallback: true,
},
function (request, accessToken, refreshToken, profile, done) {
  User.model.findOne({ googleId: profile.id }, function (err, user) {
    if (!user) {
      var newUser = new User.model()
      var data = {
        'name.fisrt': profile.name.givenName,
        'name.last': profile.name.familyName,
        'googleId': profile.id,
        'email': profile.emails ? profile.emails[0].value : profile.id + '@google.com',
        'method': 'google',
      }
      newUser.getUpdateHandler().process(data, function (err) {
        if (err) {
          return done(err, false)
        }
        return done(err, newUser)
      })
    } else
      return done(err, user)
  })
}
))

module.exports = passport
