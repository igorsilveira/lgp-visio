var keystone = require('keystone')
var router = keystone.createRouter()
var User = keystone.list('User')
var UserResult = keystone.list('UserResult')
var passport = require('passport')

var Experience = keystone.list('Experience')

/**
 * Create a User
 */
router.post('/', keystone.middleware.api, function (req, res) {
  var newUser = new User.model()
  var data = req.method === 'POST' ? req.body : req.query
  newUser.getUpdateHandler(req).process(data, function (err) {
    if (err) {
      return res.json({
        error: err,
      })
    }

    req.flash('success', 'Account created. Please sign in.')

    keystone.session.signin(
      {
        email: req.body.email,
        password: req.body.password,
      },
      req,
      res,
      function (newUser) {
        return res.redirect('/#/profile')
      },
      function (err) {
        return res.json({
          success: true,
          session: false,
          message:
            (err && err.message ? err.message : false) || 'Error signing in.',
        })
      }
    )
  })
})

router.get('/', keystone.middleware.api, function (req, res) {
  if (req.user) {
    let user = req.user
    user.score = req.user.getScore
    return res.apiResponse(user)
  } else {
    return res.apiResponse(null)
  }
})

router.post('/login', function (req, res, next) {
  passport.authenticate('local', function (err, user, info) {
    if (err) {
      return next(err)
    }

    if (!user) {
      return res.send(401, { success: false, user: null, message: 'Email ou Password estão incorretos.' })
    }
    req.login(user, function (err) {
      if (err) {
        return next(err)
      }
      return res.send({ success: true, user: user, message: 'Login feito com sucesso.' })
    })
  })(req, res, next)
})

router.get('/signout', keystone.middleware.api, function (req, res) {
  if (req.user) {
    keystone.session.signout(req, res, function () {
      return res.apiResponse('success')
    })
  } else {
    return res.apiResponse('fail')
  }
})

router.get('/login/facebook', passport.authenticate('facebook'))

router.get(
  '/auth/facebook/callback',
  passport.authenticate('facebook', {
    successRedirect: '/#/profile',
    failureRedirect: '/#/login',
    session: true,
  }),
  function (req, res) {
    res.apiResponse(req.user)
  }
)

router.get(
  '/login/google',
  passport.authenticate('google', {
    scope: [
      'https://www.googleapis.com/auth/plus.login',
      'https://www.googleapis.com/auth/plus.profile.emails.read',
    ],
  })
)

router.get(
  '/auth/google/callback',
  passport.authenticate('google', {
    successRedirect: '/#/profile',
    failureRedirect: '/#/login',
    session: true,
  }),
  function (req, res) {
    res.apiResponse(req.user)
  }
)

// ///////////////////////////////////////////////////////////////
// query que vai buscar todas as experiencias do utilizador
router.get('/:id/myExperiments', function (req, res) {
  const { id } = req.params

  let query = User.model.findOne({
    _id: id,
  })

  // .populate('steps')
  query.exec(function (err, user) {
    if (!user) return res.apiError('User does not exist!', 404)
    if (err) return res.apiError('database error', err)

    query = Experience.model
      .find({
        _id: {
          $in: user.executed,
        },
      })
      .populate({
        path: 'categories',
        match: {
          name: {
            $in: req.query.categories ? req.query.categories.split(',') : [''],
          },
        },
      })

    query.exec(function (err, experiences) {
      if (err) return res.apiError('database error', err)
      if (req.query.categories !== '') {
        experiences = experiences.filter(
          experience =>
            experience.categories.length
            === req.query.categories.split(',').length
        )
      }

      return res.apiResponse({
        experiences,
      })
    })
  })
})
// //////////////////////////////////////////////////////////////

router.get('/score', function (req, res) {
  if (!req.user)
  { return res.apiResponse('fail') }
  return res.apiResponse(req.user.getScore)
})

router.post('/update', keystone.middleware.api, function (req, res) {

  var user = req.user

  var value, cleanValue

  if (typeof req.body.value !== 'undefined') {
    value = req.body.value
    cleanValue = value.replace(/[|&;$%@"<>()+,]/g, '')

  }

  var opassword, npassword, npassword_conf

  if (typeof req.body.opassword !== 'undefined') {
    opassword = req.body.opassword
    npassword = req.body.npassword
    npassword_conf = req.body.npassword_conf
  }

  var field = req.body.field

  var searchField, updateField

  var response = { response: 'Nothing changed.' }

  if (user.email !== '')
  { searchField = { email: user.email } }
  else if (user.facebookId !== '')
  { searchField = { facebookId: user.facebookId } }
  else if (user.googleId !== '')
  { searchField = { googleId: user.googleId } }


  if (field === 'Nome Próprio') {
    updateField = { name: { first: cleanValue } }
    response = { response: 'Nome Próprio mudado.' }
  } else if (field === 'Apelido') {
    updateField = { name: { last: cleanValue } }
    response = { response: 'Apelido mudado.' }
  } else if (field === 'Palavra-Passe') {
    updateField = { password: npassword }
    response = { response: 'Palavra-Passe mudada.' }
  }

  User.model
    .find(searchField).exec(function (err, user) {
      if (err) return res.json({ response: 'Invalid input.' })

      if (field != 'Palavra-Passe') {

        if (cleanValue.length < 3)
        { return res.json({ response: 'Name must have at least 3 characters.' }) }
        else if (cleanValue.length > 12)
        { return res.json({ response: 'Name must not have more than 12 characters.' }) }
        else {
          User.updateItem(user[0], updateField, function (err) {
            if (err) return res.json({ response: 'Invalid input.' })

            return res.apiResponse(response)
          })
        }
      } else {

        user[0]._.password.compare(opassword, function (err, result) {
          if (err) return res.json({ response: err.detail.password.error })

          if (!result) {
            return res.json({ response: 'Old password doesn\'t match.' })
          } else {
            if (npassword === npassword_conf) {

              User.updateItem(user[0], updateField, function (err) {
                if (err) return res.json({ response: err.detail.password.error })
                return res.apiResponse(response)
              })

            } else
            { return res.json({ response: 'New password doesn\'t match confirmation.' }) }
          }
        })

      }

    })
})


router.post('/updateAvatar', keystone.middleware.api, function (req, res) {

  req.user.getUpdateHandler(req).process(req.body, {
    flashErrors: true,
    logErrors: true,
    files: req.files,
    user: req.user,
  }, function (err) {
    if (err) {
      res.json({ response: 'Error Occurred' })
    } else {
      req.user._.avatar.scale(200, 200)
      req.user.save()
      return res.json({ response: 'Avatar mudado.' })
    }
  })

})

router.post('/completeExperience', function (req, res) {
  if (!req.user)
  { return res.status(503).json({ response: 'Permission Denied' }) }
  if (!req.body)
  { return res.json({ response: 'Body doesn\'t exists', success: false }) }

  const searchField = {user: req.user._id, experience: req.body.experience}
  UserResult.model
    .findOne(searchField)
    .exec(function(err, userResult) {
      if (err) {
        return res.json({ response: 'Error occurred updating the result', success: false })
      }

      req.body.user = req.user._id
      if(!userResult)
      {
        let userResult = new UserResult.model()
        userResult.getUpdateHandler(req)
          .process(req.body, function (err) {
            if (err) {
              return res.json({ response: 'Error occurred creating new result', success: false })
            }
            return res.json({ response: 'Image Uploaded', success: true })
          })
      } else {
        userResult.getUpdateHandler(req).process(req.body, function (err) {
          if (err) {
            return res.json({ response: 'Error occurred updating the result', success: false })
          }
          return res.json({ response: 'Image Uploaded', success: true })
        })
      }
    })

})

router.get('/result/:experienceId', function (req, res) {
  const searchField = {user: req.user._id, experience: req.params.experienceId}
  UserResult.model
    .findOne(searchField)
    .exec(function(err, userResult) {
      if (err || !userResult) {
        return res.json({ response: 'Error Occurred, result not found', success: false })
      }
      return res.apiResponse(userResult)
    })
})

module.exports = router
