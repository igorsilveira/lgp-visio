var keystone = require('keystone')

/**
 * List Threads
 */

// Getting our thread model
var Thread = keystone.list('Thread')

// Creating the API end point
var listAll = function (req, res) {
  // Querying the data this works similarly to the Mongo db.collection.find() method
  Thread.paginate({
    page: req.query.page || 1,
    perPage: 10,
    maxPages: 100,
  })
    .sort(req.query.sort || null, req.query.order || null)
    .exec(function (err, results) {
    // Make sure we are handling errors
      if (err) return res.apiError('database error', err)

      res.apiResponse({
      // Filter thread by
        threads: results,
      })
    })
}

// Creating the API end point
exports.filter = function (req, res) {
  if (req.query.filter) {
    // Querying the data this works similarly to the Mongo db.collection.find() method
    Thread.paginate({
      page: req.query.page || 1,
      perPage: 10,
      maxPages: 100,
    })
      .where(req.query.filter, { $in: req.query.value.split(',') })
      .sort(req.query.sort || null)
      .exec(function (err, results) {
        if (err) return res.apiError('database error', err)

        res.apiResponse({
          threads: results,
        })
      })
  } else {
    listAll(req, res)
  }
}

exports.thread = function (req, res) {
  Thread.model.findById(req.params.threadId).populate('author experiencies')
    .exec(function (err, result) {
      // Make sure we are handling errors
      if (err) return res.apiError('database error', err)

      delete result.comments
      res.apiResponse({
        // Filter thread by
        thread: result,
      })
    })
}
