var keystone = require('keystone')

/**
 * List Categories
 */

// Getting our category model
var Category = keystone.list('Category')

// Creating the API end point
exports.list = function (req, res) {
  // Querying the data this works similarly to the Mongo db.collection.find() method
  Category.model
    .find(function (err, items) {
      // Make sure we are handling errors
      if (err) return res.apiError('database error', err)
      res.apiResponse({
        // Filter category by
        category: items,
      })

      // Using express req.query we can limit the number of categories returned by setting a limit property in the link
      // This is handy if we want to speed up loading times once our category collection grows
    })
    .limit(Number(req.query.limit))
}
