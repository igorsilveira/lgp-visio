var keystone = require('keystone')
var Comments = keystone.list('Comment')
var Thread = keystone.list('Thread')

/**
 * Create a Comment
 */
exports.create = function (req, res) {
  if (!req.user) {
    req.flash('error', 'Please sign in to access this page.')
    res.redirect('/signin')
  } else {
    var data = req.method === 'POST' ? req.body : req.query
    if (!data.value) {
      req.flash('error', 'Invalid data')
      return res.apiResponse({
        comment: null,
      })
    }

    Comments.model.create(
      {
        value: data.value,
        author: req.user._id,
        thread: req.params.threadId,
      }, function (err, newComment) {

        Comments.model.findById(newComment.id)
          .populate('author')
          .exec(function (err, result) {
            // Make sure we are handling errors
            if (err) return res.apiError('database error', err)

            req.flash('success', 'Comment added in success')
            return res.apiResponse({
              comment: result,
            })
          })
      })
  }
}


exports.comments = function (req, res) {
  Thread.model.findById(req.params.threadId)
    .exec(function (err, result) {
    // Make sure we are handling errors
      if (err) return res.apiError('database error', err)

      Comments.paginate({
        page: req.query.page || 1,
        perPage: 3,
        maxPages: 100,
      })
        .where('_id', { $in: result.comments })
        .where('approved').equals(true)
        .populate('author')
        .exec(function (err, results) {
          if (err) return res.apiError('database error', err)

          return res.apiResponse({
            comments: results,
          })
        })
    })
}


exports.vote = function (req, res) {
  Comments.model.findById(req.params.commentId)
    .exec(function (err, result) {
    // Make sure we are handling errors
      if (err) return res.apiError('database error', err)

      if (req.body.vote === '-') {
        result.votes -= 1
        res.json({ success: true })
      } else if (req.body.vote === '+') {
        result.votes += 1
        res.json({ success: true })
      }

      result.save()
    })
}
