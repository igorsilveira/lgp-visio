var keystone = require('keystone')

var Group = keystone.list('Group')

/**
 * Get Group by ID
 */
exports.get = function (req, res) {
  Group.model.findById(req.params.id).exec(function (err, item) {
    if (err) return res.apiError('database error', err)
    if (!item) return res.apiError('not found')

    res.apiResponse({
      item,
    })
  })
}
