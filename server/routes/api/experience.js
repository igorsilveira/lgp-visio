var keystone = require('keystone')
var router = keystone.createRouter()

/**
 * List Experiences
 */

// Getting our experience model
var Experience = keystone.list('Experience')

// Creating the API end point
router.get('/', function (req, res, next) {
  // Querying the data this works similarly to the Mongo db.collection.find() method
  Experience.model
    .find({
      state: 'published'
    })
    .populate({
      path: 'categories',
      match: {
        name: {
          $in: req.query.categories ? req.query.categories.split(',') : ['']
        }
      }
    })
    .sort({publishedAt: -1})
    .exec(function (err, experiences) {
      if (err) return res.apiError('database error', err)
      if (req.query.categories !== '') {
        experiences = experiences.filter(
          experience => experience.categories.length !== 0
        )
      }

      return res.apiResponse({
        experiences
      })
    })
})

// Creating the API end point
router.get('/:id', function (req, res) {
  // Querying the data this works similarly to the Mongo db.collection.find() method
  const {
    id
  } = req.params

  let query = Experience.model.findOne({
    _id: id,
    state: 'published'
  })

  // is the p param set, to populate steps?
  if (req.query.p === '1') {
    query.populate('steps')
    query.populate('threads')
    query.populate({
      path: 'materiais',
      populate: [{
        path: 'material'
      }]
    })
  }
  // .populate('steps')
  query.exec(function (err, experience) {
    // Make sure we are handling errors

    // if (!experience) return res.apiError('Experience does not exist!', 404)
    // if (err) return res.apiError('database error', err)

    res.apiResponse({
      // Filter experience by
      // steps: experience.steps,
      experience
    })

    // Using express req.query we can limit the number of experiences returned by setting a limit property in the link
    // This is handy if we want to speed up loading times once our experience collection grows
  })
})

// Creating the API end point
router.get('/:id/steps', function (req, res) {
  // Querying the data this works similarly to the Mongo db.collection.find() method
  let {
    id
  } = req.params
  Experience.model
    .findOne({
      _id: id,
      state: 'published'
    })
    .populate('steps')
    .exec(function (err, experience) {
      // Make sure we are handling errors

      if (!experience) return res.apiError('Experience does not exist!', 404)
      if (err) return res.apiError('database error', err)
      res.apiResponse({
        // Filter experience by
        steps: experience.steps
      })

      // Using express req.query we can limit the number of experiences returned by setting a limit property in the link
      // This is handy if we want to speed up loading times once our experience collection grows
    })
})

// Creating the API end point
router.get('/:id/assets', function (req, res) {
  // Querying the data this works similarly to the Mongo db.collection.find() method
  let {
    id
  } = req.params

  Experience.model
    .findOne({
      _id: id,
      state: 'published'
    })
    .populate('assets')
    .exec(function (err, experience) {
      // Make sure we are handling errors

      if (!experience) return res.apiError('Experience does not exist!', 404)
      if (err) return res.apiError('database error', err)
      res.apiResponse({
        // Filter experience by
        assets: experience.assets
      })

      // Using express req.query we can limit the number of experiences returned by setting a limit property in the link
      // This is handy if we want to speed up loading times once our experience collection grows
    })
})

// Creating the API end point
router.get('/:id/materiais', function (req, res) {
  // Querying the data this works similarly to the Mongo db.collection.find() method
  let {
    id
  } = req.params
  Experience.model
    .findOne({
      _id: id,
      state: 'published'
    })
    .populate('materiais')
    .populate('materiais.material')
    .exec(function (err, experience) {
      // Make sure we are handling errors
      if (!experience) return res.apiError('Experience does not exist!', 404)
      if (err) return res.apiError('database error', err)
      res.apiResponse({
        // Filter experience by
        materiais: experience.materiais
      })

      keystone
        .list('ExperienceMaterial')
        .model.find()
        .populate('material')
        .exec(function (err, materiall) {
          // Make sure we are handling errors

          if (!materiall) return res.apiError('Material does not exist!', 404)
          if (err) return res.apiError('database error', err)
          res.apiResponse({
            // Filter experience by
            material: materiall.material
          })
        })

      // Using express req.query we can limit the number of experiences returned by setting a limit property in the link
      // This is handy if we want to speed up loading times once our experience collection grows
    })
})

module.exports = router
