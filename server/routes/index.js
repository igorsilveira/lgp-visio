/**
 * This file is where you define your application routes and controllers.
 *
 * Start by including the middleware you want to run for every request;
 * you can attach middleware to the pre('routes') and pre('render') events.
 *
 * For simplicity, the default setup for route controllers is for each to be
 * in its own file, and we import all the files in the /routes/views directory.
 *
 * Each of these files is a route controller, and is responsible for all the
 * processing that needs to happen for the route (e.g. loading data, handling
 * form submissions, rendering the view template, etc).
 *
 * Bind each route pattern your application should respond to in the function
 * that is exported from this module, following the examples below.
 *
 * See the Express application routing documentation for more information:
 * http://expressjs.com/api.html#app.VERB
 */

var keystone = require('keystone')
var middleware = require('./middleware')
var adminRoutes = require('./admin/adminRoutes')
var importRoutes = keystone.importer(__dirname)
var passport = require('./passport')

// Common Middleware
keystone.pre('routes', middleware.initLocals)
keystone.pre('routes', passport.initialize())
keystone.pre('routes', passport.session())
keystone.pre('admin', adminRoutes.restrictModels)
keystone.pre('admin', adminRoutes.restrictRoutes)
keystone.pre('admin', adminRoutes.restrictAPIRoutes)
keystone.pre('render', middleware.flashMessages)

// Import Route Controllers
var routes = {
  views: importRoutes('./views'),
  api: importRoutes('./api'),
}

// Setup Route Bindings
exports = module.exports = function (app) {
  // Views
  app.get('/', routes.views.index)

  // NOTE: To protect a route so that only admins can see it, use the requireUser middleware:
  // app.get('/protected', middleware.requireUser, routes.views.protected);

  // Get access to the API route in our app

  app.use('/api/experiences/', keystone.middleware.api, routes.api.experience)

  app.get('/api/group/:id', keystone.middleware.api, routes.api.group.get)

  app.use('/api/user', keystone.middleware.api, routes.api.user)

  app.get('/api/threads/', keystone.middleware.api, routes.api.thread.filter)
  app.get('/api/threads/:threadId/', keystone.middleware.api, routes.api.thread.thread)
  app.post('/api/threads/:threadId/comment', keystone.middleware.api, routes.api.comment.create)
  app.get('/api/threads/:threadId/comments', keystone.middleware.api, routes.api.comment.comments)

  app.post('/api/comments/:commentId', keystone.middleware.api, routes.api.comment.vote)

  app.get('/api/category/', keystone.middleware.api, routes.api.category.list)

  app.get('/api/asset/list', keystone.middleware.api, routes.api.asset.list)
  app.get('/api/asset/:id', keystone.middleware.api, routes.api.asset.get)
  app.all('/api/asset/:id/update', keystone.middleware.api, routes.api.asset.update)
  app.all('/api/asset/create', keystone.middleware.api, routes.api.asset.create)
  app.get('/api/asset/:id/remove', keystone.middleware.api, routes.api.asset.remove)
}
