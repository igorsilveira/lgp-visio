import React, { Component } from 'react'
import axios from 'axios'
// We gonna use lodash to map over our experience object
import _ from 'lodash'
import ExperienceCard from '../components/ExperienceCard'
import Grid from '@material-ui/core/Grid'

import { withStyles } from '@material-ui/core/styles'
import CircularLoading from '../components/CircularLoading'
import { Animated } from 'react-animated-css'

class Experiences extends Component {
  constructor (props) {
    super(props)
    this.classes = props.classes
    this.state = {
      loading: true,
      experiences: null,
    }
    // Bind our render experience to function so we can use it in the render method
    this.renderExperiences = this.renderExperiences.bind(this)
  }

  // Fetch experiences when component is mounted
  componentDidMount () {
    this.getExperiences()
  }

  componentDidUpdate (prevProps) {
    if (prevProps.currentCategories !== this.props.currentCategories) {
      this.loading = true
      this.getExperiences()
    }
  }

  getExperiences () {
    const { url, currentCategories } = this.props // '/api/experiences';
    let url2 = `${url}?categories=${currentCategories.join()}`
    axios.get(url2)
      .then(response =>
        this.setState({
          loading: false,
          experiences: response.data.experiences,
        })
      )
      .catch(err => console.error('Error getting Experiences', err))
  }
  renderExperiences () {
    return _.map(this.state.experiences, (experience,index) => {

      return (
        <Grid item xs={12} sm={12} md={6} lg={6} xl={4} key={experience._id}>
          <Animated animationIn="fadeInUp" animationInDelay={index*300+400} isVisible={true}>
            <ExperienceCard experience={experience} />
          </Animated>
        </Grid>
      )

         
    })
  }
  render () {
    const { classes } = this.props
    // If data is still loading
    if (this.state.loading) {
      return (
        <CircularLoading />
      )
    }
    // Show experience once data is loaded

    return (
      <div>


        <Grid className={classes.experiencesContainer} container spacing={24}>
          {this.renderExperiences()}
        </Grid>
      </div>)

  }
}

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
})

export default withStyles(styles)(Experiences)
