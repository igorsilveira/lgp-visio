import React, { Component } from 'react'
import ReactDOM from 'react-dom'

import { withStyles } from '@material-ui/core/styles'

import Paper from '@material-ui/core/Paper'
import { Button, IconButton, Grid, Fab } from '@material-ui/core'
import Popover from '@material-ui/core/Popover'
import Typography from '@material-ui/core/Typography'
import CloseIcon from '@material-ui/icons/Close'
import ArrowLeft from '@material-ui/icons/ArrowLeft'
import ArrowRight from '@material-ui/icons/ArrowRight'
import gif1 from './resources/tutorial_open.gif'
import gif2 from './resources/tutorial.gif'
import gif3 from './resources/tutorial_close.gif'
import withWidth, { isWidthUp } from '@material-ui/core/withWidth'
import Zoom from '@material-ui/core/Zoom'
import classNames from 'classnames'
import SplitText from 'react-pose-text'
import PosedText from '../components/PosedText'

const charPoses = {
  exit: { opacity: 0 },
  enter: { opacity: 1 },
}

const popoverContent = [
  'Olá aqui poderás aprender mais sobre a experiência para a poderes executar.',
  'Começa por ver se tens os materiais necessários e prepara-os...',
  'Depois de saber do que precisas, dá início à tua experiência e segue todos os passos, diverte-te e aprende com a tua execução.'
]

const styles = theme => ({
  closeButtonDialog: {
    position: 'absolute',
    top: theme.spacing.unit / 2,
    right: theme.spacing.unit / 2,
  },
  gif: {
    position: 'absolute',
    left: 0,
    zIndex: 999,
    top: '10rem',
    width: '15%',
    transition: 'top 500ms ease-in-out',
  },
  gifStep2: {
    left: 'auto',
    transform: 'scaleX(-1)',
    right: 0,
  },
  fab: {
    fontWeight: 'bold',
    position: 'fixed',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 2,
  },
  popover: {
    position: 'relative',
    paddingTop: theme.spacing.unit * 2,
    paddingLeft: theme.spacing.unit * 2,
    paddingRight: theme.spacing.unit * 6,
    paddingBottom: 'calc(16px + ' + theme.spacing.unit + 'px)',
    clipPath:
      'polygon(0% 0%, 100% 0%, 100% calc(100% - 16px), 40px calc(100% - 16px), 16px 100%, 16px calc(100% - 16px), 0% calc(100% - 16px))',
    maxWidth: '600px',
  },
  popoverStep2: {
    clipPath:
      'polygon(0% 0%, 100% 0%, 100% calc(100% - 16px), calc(100% - 40px) calc(100% - 16px), calc(100% - 40px) 100%, calc(100% - 56px) calc(100% - 16px), 0% calc(100% - 16px))',
  },
  popoverContainer: {
    backgroundColor: 'transparent',
    boxShadow: 'none',
    filter: 'drop-shadow(0px 8px 10px rgba(0,0,0,0.14))',
  },
  dropShadowGif: {
    filter: 'drop-shadow(0px 5px 4px rgba(0,0,0,0.5))',
  },
})

class ExperienceTutorial extends Component {
  state = {
    anchorEl: null,
    openPopOver: true,
    open: false,
    tutorialStep: 0,
    gif: gif1,
  };

  componentDidMount () {
    if (!this.props.tutorial)
    { setTimeout(() => {
      this.setState({
        anchorEl: this.refs.GifContainer,
        open: true,
        gif: gif1,
      })
      this.props.handleChangeStage(0)
    }, 1000) }
    setTimeout(() => {
      this.setState({
        gif: gif2,
      })
    }, 2500)

    const imgs = [gif1, gif2, gif3]
    imgs.forEach((gif) => {
      const img = new Image()
      img.src = gif
    })
  }

  handlePOPClick = event => {
    this.setState({
      gif: gif1,
      open: true })

    this.props.handleChangeStage(this.state.tutorialStep)
    setTimeout(() => {
      this.setState({
        anchorEl: this.refs.GifContainer,
        openPopOver: true,
        gif: gif2,
      })


    }, 500)
  };

  handlePOPClose = () => {
    this.setState({
      openPopOver: false,
    })

  };

  closeTutorial = () => {
    this.handlePOPClose()

    this.setState({
      gif: gif3 })
    setTimeout(() => {
      this.setState({ open: false, tutorialStep: 0 })
    }, 600)

    this.props.handleChangeStage(-1)
  }

  tutorialShowStep_0 () {
    const scrollToTop = () => {
      const c = document.documentElement.scrollTop || document.body.scrollTop
      if (c > 0) {
        window.requestAnimationFrame(scrollToTop)
        window.scrollTo(0, c - c / 8)
      }
    }
    this.props.closeMaterials()
    this.props.handleChangeStage(0)
    this.handlePOPClose()
    setTimeout(() => {
      this.setState({
        anchorEl: this.refs.GifContainer,
        openPopOver: true,
        tutorialStep: 0,
      })
    }, 600)

    this.refs.GifContainer.style.top = '10rem'
    scrollToTop()
  }

  tutorialShowStep_1 () {
    let materialsExpansion = ReactDOM.findDOMNode(this.props.openMaterials())
    this.props.handleChangeStage(1)
    this.handlePOPClose()
    setTimeout(() => {
      this.setState({
        anchorEl: this.refs.GifContainer,
        openPopOver: true,
      })
    }, 600)
    if (isWidthUp('md', this.props.width)) {
      this.refs.GifContainer.style.top
        = 'calc('
        + materialsExpansion.offsetTop
        + 'px - '
        + this.refs.GifContainer.offsetHeight
        + 'px)'
    } else {
      let bottomMaterials
        = materialsExpansion.offsetTop + materialsExpansion.offsetHeight + 75
      this.refs.GifContainer.style.top
        = 'calc('
        + bottomMaterials
        + 'px + '
        + this.refs.GifContainer.offsetHeight
        + 'px)'
    }
    materialsExpansion.scrollIntoView({ behavior: 'smooth' })
  }

  tutorialShowStep_2 () {
    const scrollToTop = () => {
      const c = document.documentElement.scrollTop || document.body.scrollTop
      if (c > 0) {
        window.requestAnimationFrame(scrollToTop)
        window.scrollTo(0, c - c / 8)
      }
    }

    this.props.closeMaterials()
    this.props.handleChangeStage(2)
    this.handlePOPClose()
    setTimeout(() => {
      this.setState({
        anchorEl: this.refs.GifContainer,
        openPopOver: true,
      })
    }, 600)
    this.refs.GifContainer.style.top = '50vh'
    scrollToTop()
  }

  handleNextStep = event => {
    event.preventDefault()

    switch (this.state.tutorialStep) {
    case 0:
      this.tutorialShowStep_1()
      break
    case 1:
      this.tutorialShowStep_2()
      break
    default:
      break
    }

    const currentStep = this.state.tutorialStep + 1

    setTimeout(() => { this.setState({
      tutorialStep: currentStep,
    }) }, 150)

  };

  handlePreviousStep = event => {
    event.preventDefault()

    switch (this.state.tutorialStep) {
    case 1:
      this.tutorialShowStep_0()
      break
    case 2:
      this.tutorialShowStep_1()
      break
    default:
      break
    }

    const currentStep = this.state.tutorialStep - 1

    setTimeout(() => { this.setState({
      tutorialStep: currentStep,
    }) }, 150)

    if (this.state.tutorialStep === 3)
    { this.props.closeTutorial() }
  };

  render () {
    const { openPopOver } = this.state
    const { classes, theme } = this.props

    const transitionDuration = {
      enter: theme.transitions.duration.enteringScreen,
      exit: theme.transitions.duration.leavingScreen,
    }

    return (
      <div>
        {!this.state.open
        && <Zoom
          key={'help-btn'}
          timeout={transitionDuration}
          in={true}
          unmountOnExit
        >
          <Fab className={classes.fab} color="primary" onClick={this.handlePOPClick}>
            ?
          </Fab>
        </Zoom>}
        {this.state.open
        && <div
          id="GifContainer"
          ref="GifContainer"
          className={classNames(classes.gif, this.state.tutorialStep === 2 ? classes.gifStep2 : null)}
        >
          <img
            src={this.state.gif}
            width="100%"
            style={{ transform: `scaleX(${this.state.transformX})` }}
            className={classes.dropShadowGif}
            onClick={this.handlePOPClick}
            ref={elem =>
              !this.state.anchorEl ? this.setState({ anchorEl: elem }) : null
            }
          />
          <div>
            <Popover
              id="tutorial-popper"
              open={openPopOver}
              anchorEl={this.state.anchorEl}
              onClose={this.closeTutorial}
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'center',
              }}
              transformOrigin={{
                vertical: isWidthUp('md', this.props.width)
                  ? 'center'
                  : 'bottom',
                horizontal: this.state.tutorialStep !== 2 ? 'left' : 'right',
              }}
              PaperProps={{
                classes: { root: classes.popoverContainer },
                onClick: event => {
                  event.preventDefault()
                },
              }}
            >
              <Paper className={classNames(classes.popover, this.state.tutorialStep === 2 ? classes.popoverStep2 : null)}>
                <Grid container>
                  <IconButton
                    color="primary"
                    onClick={this.closeTutorial}
                    aria-label="Close"
                    className={classes.closeButtonDialog}
                  >
                    <CloseIcon />
                  </IconButton>
                  <Grid item xs={12} className="mb-1">
                    <PosedText initialPose="exit" pose="enter">
                      <Typography variant={'body1'} component="div">
                        <SplitText charPoses={charPoses}>{popoverContent[this.state.tutorialStep]}</SplitText>
                      </Typography>
                    </PosedText>
                  </Grid>
                  <Grid container item xs={12} justify="flex-end">
                    {this.state.tutorialStep > 0 ? (
                      <Button
                        onClick={this.handlePreviousStep}
                        aria-label="Close"
                      >
                        <ArrowLeft />
                        <Typography>Anterior</Typography>
                      </Button>
                    ) : null }
                    {this.state.tutorialStep <= popoverContent.length - 2 ? (
                      <Button onClick={this.handleNextStep} aria-label="Close">
                        <Typography>Próximo</Typography>
                        <ArrowRight />
                      </Button>
                    ) : null }
                  </Grid>
                </Grid>
              </Paper>
            </Popover>
          </div>
        </div>}
      </div>
    )
  }
}

export default withWidth()(withStyles(styles, { withTheme: true })(ExperienceTutorial))
