import React, { Component } from 'react'
import Sound from 'react-sound'
import axios from 'axios'
import store from '../store'
import extenso from 'extenso'

import CircularLoading from '../components/CircularLoading'
import ExperienceSteps from '../containers/ExperienceSteps'
import ExperienceTutorial from './ExperienceTutorial'
// Layout
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Avatar from '@material-ui/core/Avatar'
import Paper from '@material-ui/core/Paper'
import ExpansionPanel from '@material-ui/core/ExpansionPanel'
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary'
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import TrendingUp from '@material-ui/icons/TrendingUp'
import PlayArrowIcon from '@material-ui/icons/PlayArrow'
import Pause from '@material-ui/icons/Pause'
import Timer from '@material-ui/icons/Timer'
import ViewQuilt from '@material-ui/icons/ViewQuilt'

import { withStyles } from '@material-ui/core/styles'
import { Tooltip } from '@material-ui/core'
import Button from '@material-ui/core/Button'
import Icon from '@material-ui/core/Icon'
import Modal from '@material-ui/core/Modal'
import { Link } from 'react-router-dom'


const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    flexGrow: 1,
    padding: theme.spacing.unit,
    marginTop: theme.spacing.unit * 5,
    position: 'relative',
  },
  backdrop: {
    position: 'fixed',
    height: '100%',
    width: '100%',
    backgroundColor: 'rgba(0,0,0,0.3)',
    left: 0,
    top: 0,
  },
  toolbar: theme.mixins.toolbar,
  experienceName: {
    fontWeight: '800',
    color: '#007E9F',
  },
  soundButton: {
    color: 'white',
    marginBottom: theme.spacing.unit * 2
  },
  soundButtonIcon: {
    marginLeft: theme.spacing.unit * 2
  },
  categoryList: {
    fontSize: '1.5em',
    marginTop: '.4em',
    fontWeight: 'lighter',
  },
  stats: {
    background: 'transparent',
  },
  statsTitle: {
    fontWeight: 'lighter',
    color: '#AAAAAA',
  },
  statsBody: {
    fontWeight: '800',
    color: '#AAAAAA',
  },
  statsIcon: {
    fontWeight: '800',
    color: '#AAAAAA',
  },
  description: {
    fontWeight: 'lighter',
  },
  categoryAvatar: {
    border: 'solid 4px',
    height: 70,
    width: 70,
  },
  listTitle: {
    fontWeight: '800',
    color: '#007E9F',
  },
  '@keyframes resultTab': {
    from: { right: -200 },
    to: { right: 0 }
  },
  '@keyframes resultTabBounce': {
    from: { transform: 'scale(1)' },
    to: { transform: 'scale(1.2)' },
  },
  resultTab: {
    borderRadius: '0 0 5px 5px',
    transformOrigin: 'top right',
    transform: 'rotate(90deg)',
    fontWeight: 'bold',
    position: 'fixed',
    bottom: '45%',
    animation: '1s resultTab',
    animationDelay: '0.5',
    animationTimingFunction: 'ease-out',
    animationFillMode: 'forwards',
    backgroundColor: '#49C087',
    color: 'white'
  },
  resultTabBounce: {
    animation: '1s resultTabBounce',
    animationDelay: '1.5',
    animationTimingFunction: 'ease-out',
  },
  expansionPanel: {
    marginTop: theme.spacing.unit * 3,
  },
  expansionPanelDetails: {
    flexDirection: 'column',
  },
  customBadge: {
    paddingLeft: 10,
  },
  imageGridRoot: {
    overflow: 'hidden',
    height: 0,
    paddingTop: '55vh',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
  },
  gridList: {
    width: '100%',
    height: 500,
  },
  materialList: {
    width: 40,
    height: 40,
    marginTop: 5,
    marginRight: 10,
  },
  overBackdrop: {
    borderRadius: 10,
    position: 'relative',
    transition: 'all .3s ease-out',
    zIndex: 999,
  },
  descriptionBackdrop: {
    backgroundColor: 'white',
    padding: theme.spacing.unit * 2,
    fontWeight: 'normal'
  },
  paper: {
    position: 'absolute',
    maxWidth: '90vw',
    maxHeight: '90vh',
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    borderRadius: 5,
    outline: 'none',
    top: '10%',
    left: '10%',
    transform: 'translate(-5%, -5%)',
  },
  resultImage: {
    borderRadius: 5,
    maxWidth: '90vw',
    maxHeight: '90vh',
  }
})

class Experience extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      resultOpen: false,
      experience: null,
      categories: null,
      materialsExpansion: false,
      animationStage: -1,
      user: null,
      music: true,
      resultUrl: null
    }
    // Bind our render experience to function so we can use it in the render method
    this.renderExperience = this.renderExperience.bind(this)
  }

  handleMusicStatus = () => {
    const status = this.state.music
    this.setState({ music: !status })
  }


  createSoundComponent = (experience) => {
    if (experience.soundDescription) {
      return (
        <Sound
          url={'/uploads/files/music/' + experience.soundDescription.filename}
          playStatus={this.state.music ? Sound.status.PLAYING : Sound.status.PAUSED}
          onLoading={this.handleSongLoading}
          onPlaying={this.handleSongPlaying}
          onFinishedPlaying={this.handleSongFinishedPlaying}
          autoLoad={true}
        />)
    }
  }

  handleChange = () => {
    let currentState = store.getState()
    if (this.state.categories !== currentState.addCategories) {
      this.setState({ categories: currentState.addCategories })
    }
    if (this.state.user !== currentState.addUser) {
      this.setState({ user: currentState.addUser })
    }
  };

  handleOpenResult =
    () => {
      this.setState({ resultOpen: true })
    }

  handleCloseResult =
    () => {
      this.setState({ resultOpen: false })
    }
  openMaterialsExpansion = () => {
    this.setState({ materialsExpansion: true })
    return this.refs.Materials
  }

  closeMaterialsExpansion = () => {
    this.setState({ materialsExpansion: false })
  }

  handleChangeStage(stage) {
    this.setState({ animationStage: stage })
  }

  // Handle material panel on click
  handleExpansionToggle = () => {
    const lastState = this.state.materialsExpansion
    this.setState({ materialsExpansion: !lastState })
  }

  fetchResult = () => {
    const { id } = this.props

    const API_URL_IMAGE = `api/user/result/${id}`
    axios.get(API_URL_IMAGE)
      .then(response => {
        if (response.data.resultImage) {
          this.setState({ resultUrl: response.data.resultImage.secure_url })
        }
      })
      .catch(err => console.error('Error getting experience result', err))
  }

  // Fetch experiences when component is mounted
  componentDidMount() {
    const { id } = this.props

    store.subscribe(this.handleChange)

    this.handleChange()

    const API_URL = `api/experiences/${id}?p=1`
    axios.get(API_URL)
      .then(response => {
        if (response.data.experience)
          this.setState({ loading: false, experience: response.data.experience })

      })
      .catch(err => console.error('Error getting Experience', err)
      )

    this.fetchResult()
  }

  createResultTab = () => {
    const { resultUrl, resultOpen } = this.state
    const { classes } = this.props

    if (resultUrl) {

      return (
        <div>

          <Modal
            aria-labelledby="result-image"
            open={resultOpen}
            onClose={this.handleCloseResult}
          >
            <div className={classes.paper}>
              <img src={resultUrl} className={classes.resultImage} />
            </div>
          </Modal>
          <Button className={classes.resultTab} onClick={this.handleOpenResult} >
            O Meu resultado
          </Button>
        </div>
      )
    }
  }

  // Function to render our experience
  renderExperience() {
    const { experience, categories } = this.state
    const { classes, id } = this.props

    // Check if there is an image to be displayed
    function createImagesGrid() {
      if (experience.image) {
        return (
          <Grid item>
            <Paper className={classes.imageGridRoot} elevation={1} style={{ backgroundImage: `url(${experience.image.url})` }} />
          </Grid>
        )
      }
    }

    // Get the html for our experience materials
    function createMaterialsList() {
      if (experience.materiais) {
        return (
          <div className={`${classes.expansionPanel} ${this.state.animationStage === 1 ? classes.overBackdrop : ''}`} >
            <ExpansionPanel expanded={this.state.materialsExpansion} ref="Materials" id="Materials" onClick={this.handleExpansionToggle}>
              <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                <Typography className={classes.listTitle} variant="h6" inline>
                  Materiais
                  <Typography
                    variant="body1"
                    inline
                    color="textSecondary"
                    className={classes.customBadge}
                  >
                    {`(${experience.materiais.length})`}
                  </Typography>
                </Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails className={classes.expansionPanelDetails}>
                {experience.materiais.map((material, index) => (
                  <Typography key={index} variant="body1" paragraph>

                    <img src={material.material.image.url} className={classes.materialList} />
                    {material.quantity}
                    {material.type}{' - '}
                    {material.name}
                  </Typography>
                ))}
              </ExpansionPanelDetails>
            </ExpansionPanel>
          </div>
        )
      } else {
        return
      }
    }
    // Get the html for our experience materials
    function createThreadsList() {
      if (experience.threads) {
        return (
          <div className={classes.expansionPanel}>
            <ExpansionPanel>
              <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                <Typography className={classes.listTitle} variant="h6" inline>
                  Tópicos
                  <Typography
                    variant="body1"
                    inline
                    color="textSecondary"
                    className={classes.customBadge}
                  >
                    {`(${experience.threads.length})`}
                  </Typography>
                </Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails className={classes.expansionPanelDetails}>
                {experience.threads.map(thread => (
                  <Link to={`/thread/${thread._id}`}
                    key={thread.key}>
                    <Typography variant="body1"
                      color="textSecondary">
                      {thread.name}
                    </Typography>
                  </Link>
                ))}
              </ExpansionPanelDetails>
            </ExpansionPanel>
          </div>
        )
      } else {
        return
      }
    }


    function createCategoriesString() {
      if (experience.categories) {
        let categoriesNames = []
        experience.categories.map(cat => {
          categoriesNames.push(categories[cat].name)
        })
        return categoriesNames.join(', ')
      }
    }

    function createStatsList() {
      if (experience) {
        return (
          <Paper className={classes.stats} elevation={0} style={{ marginTop: 40 }}>
            <Grid item xs container direction="row" spacing={16}>
              <Grid
                item
                xs
                container
                direction="column"
                justify="center"
                alignItems="center"
                alignContent="center"
              >
                <Grid item xs>
                  <TrendingUp className={classes.statsIcon} />
                </Grid>
                <Grid item xs>
                  <Typography variant="h6" className={classes.statsTitle} align="center">
                    Dificuldade
                  </Typography>
                  <Typography
                    variant="body2"
                    align="center"
                    className={classes.statsBody}
                    style={{ textTransform: 'capitalize' }}
                  >
                    {experience.difficulty}
                  </Typography>
                </Grid>
              </Grid>
              <Grid
                item
                xs
                container
                direction="column"
                justify="center"
                alignItems="center"
                alignContent="center"
              >
                <Grid item xs>
                  <ViewQuilt className={classes.statsIcon} />
                </Grid>
                <Grid item xs>
                  <Typography variant="h6" className={classes.statsTitle} align="center">
                    Passos
                  </Typography>
                  <Typography
                    variant="body2"
                    align="center"
                    className={classes.statsBody}
                    style={{ textTransform: 'capitalize' }}
                  >
                    {extenso(experience.steps.length, { locale: 'pt' })}
                  </Typography>
                </Grid>
              </Grid>
              <Grid
                item
                xs
                container
                direction="column"
                justify="center"
                alignItems="center"
                alignContent="center"
              >
                <Grid item xs>
                  <Timer className={classes.statsIcon} />
                </Grid>
                <Grid item xs>
                  <Typography variant="h6" className={classes.statsTitle} align="center">
                    Duração
                  </Typography>
                  <Typography variant="body2" className={classes.statsBody} align="center">
                    {experience.duration
                      ? `~${Math.floor(experience.duration)} Minutos`
                      : 'N/A'}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          </Paper>
        )
      }
    }

    function createCategoryAvatar(id) {
      if (id) {
        return (
          <Avatar
            src={categories[id].icon.url}
            className={classes.categoryAvatar}
            style={{ borderColor: categories[id].color }}
          >
            {categories[id].name.charAt(0)}
          </Avatar>
        )
      }
      return <div />
    }
    return (

      <section className={classes.root}>

        {this.createSoundComponent(experience)}
        <div className={classes.toolbar} />
        {this.createResultTab()}
        <Grid container direction="column" key={experience._id}>
          <Grid
            item
            container
            direction="row"
            spacing={16}
            justify="space-around">

            <Grid item md={4}>
              <Grid
                item
                container
                direction="row"
                style={{ marginBottom: 30 }}
                justify="space-between">

                <Grid
                  item
                  // Keep good page spacing
                  xs={10}
                  container
                  direction="column">
                  <Grid
                    item
                    container
                    direction="row"
                    alignContent="center"
                  >
                    {experience.categories.map((cat, index) => (
                      <Grid item key={`cat-${index}`}>
                        {createCategoryAvatar(cat)}
                      </Grid>
                    ))}
                  </Grid>
                  <Grid item>
                    <Typography className={classes.experienceName} variant="h3" component="h1">
                      {experience.name}
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Typography
                      variant="h4"
                      component="h2"
                      className={classes.categoryList}
                      color="textSecondary">

                      {createCategoriesString()}
                    </Typography>
                  </Grid>

                </Grid>
              </Grid>
              {experience.soundDescription &&
                <Tooltip title={`${this.state.music ? 'Parar' : 'Tocar'}`}>
                  <Button variant={'contained'} className={classes.soundButton} style={{ backgroundColor: `${this.state.music ? '#49C087' : '#E05C46'}` }} size={'small'} onClick={this.handleMusicStatus}>
                    {this.state.music ? 'A tocar...' : 'Pausada...'}

                    <Icon className={classes.soundButtonIcon}>
                      {this.state.music ? <PlayArrowIcon /> : <Pause />}
                    </Icon>
                  </Button>
                </Tooltip>}
              <Typography className={`${classes.description} ${this.state.animationStage === 0 ? `${classes.overBackdrop} ${classes.descriptionBackdrop}` : ''}`} variant="body1">{experience.description}</Typography>
              {createStatsList()}
              {createMaterialsList.bind(this)()}
              {createThreadsList.bind(this)()}
            </Grid>

            <Grid container item md={7} justify="flex-start" direction="column">
              {createImagesGrid()}

              <Grid item container direction="row" justify="center">
                <Paper style={{ marginTop: 30, boxShadow: 'none', background: 'none' }}>
                  <Grid item xs>
                  </Grid>
                  <Grid item xs
                    className={`${this.state.animationStage === 2 ? `${classes.overBackdrop} ${classes.descriptionBackdrop}` : ''}`}>

                    <ExperienceSteps
                      steps={experience.steps}
                      experience={experience}
                      updateResult={this.fetchResult}
                    />
                  </Grid>
                  <Grid item xs>
                  </Grid>
                </Paper>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        {this.state.animationStage !== -1 &&
          <div className={classes.backdrop} />}
        <ExperienceTutorial
          handleChangeStage={this.handleChangeStage.bind(this)}
          openMaterials={this.openMaterialsExpansion.bind(this)}
          closeMaterials={this.closeMaterialsExpansion.bind(this)}
          tutorial={!this.state.user ? false : this.state.user.tutorial} />


      </section>
    )
  }

  render() {
    const { loading } = this.state

    // If data is still loading
    if (loading || this.state.categories.length === 0) {
      return <CircularLoading />
    }

    // Show experience once data is loaded
    return (
      this.renderExperience()
    )
  }
}

export default withStyles(styles)(Experience)
