import React from 'react'

import { withStyles } from '@material-ui/core/styles'

import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogContent from '@material-ui/core/DialogContent'
import Slide from '@material-ui/core/Slide'
import Stepper from '@material-ui/core/Stepper'
import Step from '@material-ui/core/Step'
import StepContent from '@material-ui/core/StepContent'
import StepLabel from '@material-ui/core/StepLabel'
import Typography from '@material-ui/core/Typography'
import Paper from '@material-ui/core/Paper'
import Divider from '@material-ui/core/Divider'
import IconButton from '@material-ui/core/IconButton'
import CloseIcon from '@material-ui/icons/Close'
import CloudUploadIcon from '@material-ui/icons/CloudUpload'
import AddIcon from '@material-ui/icons/Add'

import GridList from '@material-ui/core/GridList'
import GridListTile from '@material-ui/core/GridListTile'

import ArrowForward from '@material-ui/icons/ArrowForwardIos'
import withMobileDialog from '@material-ui/core/withMobileDialog'
import { Fab, Grid, LinearProgress } from '@material-ui/core'
import PosedText from '../components/PosedText'
import SplitText from 'react-pose-text'

const stepImageHeight = 280

const styles = theme => ({
  root: {
    height: '100%',
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between'
  },
  button: {
    marginTop: theme.spacing.unit * 2
  },
  instructions: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit
  },
  startButton: {
    width: '100%',
    padding: 15,
    backgroundColor: '#49C087',
    color: 'white',
    borderRadius: '5em',
    fontWeight: 'bold',

    '&:hover': {
      backgroundColor: '#005578'
    },
    '&:focus': {
      outline: 'none'
    }
  },
  gridList: {
    width: '100%',
    height: stepImageHeight
  },

  instruction: {
    margin: 10
  },
  stepContent: {
    marginBottom: 30
  },
  appBar: {
    position: 'relative',
    width: '100%'
  },
  closeButtonDialog: {
    position: 'absolute',
    top: theme.spacing.unit,
    right: theme.spacing.unit
  },
  gifTutorial: {
    position: 'absolute',
    bottom: theme.spacing.unit,
    right: theme.spacing.unit,
    width: '20%'
  },
  leftIcon: {
    marginRight: theme.spacing.unit
  },
  rightIcon: {
    marginLeft: theme.spacing.unit
  },
  submitButton: {
    color: 'white'
  },
  wrapper: {
    margin: theme.spacing.unit,
    position: 'relative'
  },
  fabProgress: {
    marginTop: theme.spacing.unit * 3,
    zIndex: 1
  }
})

const charPoses = {
  exit: { opacity: 0 },
  enter: { opacity: 1 }
}

function Transition(props) {
  return <Slide direction="up" {...props} />
}

class ExperienceSteps extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      open: false,
      gifs: null,
      gifStep: 0
    }
  }

  handleClickOpen = () => {
    this.setState({ open: true, gifStep: 0 })
    setTimeout(() => {
      this.setState({ gifStep: 1 })
    }, 400)

    setTimeout(() => {
      this.setState({ gifStep: 2 })
    }, 1500)
  };

  handleClose = () => {
    this.setState({ gifStep: 3 })

    setTimeout(() => {
      this.setState({ open: false, gifStep: 0 })
    }, 400)
  };

  componentDidMount() {
    const randomCategory = this.props.experience.categories[
      Math.floor(Math.random() * this.props.experience.categories.length)
    ]

    // const images = this.importAll(require.context(`./resources/${randomCategory}/`, false, /\.gif$/))
    const gifs = [
      '', // handle popup
      require(`./resources/${randomCategory}/1.gif`),
      require(`./resources/${randomCategory}/2.gif`),
      require(`./resources/${randomCategory}/3.gif`)
    ]

    this.setState({
      gifs: gifs
    })
  }

  render() {
    const { classes, fullScreen } = this.props
    return (
      <div>
        <Button onClick={this.handleClickOpen} className={classes.startButton}>
          Começar Agora
          <ArrowForward />
        </Button>
        <Dialog
          open={this.state.open}
          fullWidth
          maxWidth="xl"
          fullScreen={fullScreen}
          TransitionComponent={Transition}
          keepMounted
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
          id="dialogContent"
        >
          <DialogContent>
            <IconButton
              color="primary"
              onClick={this.handleClose}
              aria-label="Close"
              className={classes.closeButtonDialog}
            >
              <CloseIcon />
            </IconButton>
            <HorizontalLinearStepper
              {...this.props}
              open={this.state.open}
              gif={this.state.gifs ? this.state.gifs[this.state.gifStep] : null}
              handleClose={this.handleClose}
            />
          </DialogContent>
        </Dialog>
      </div>
    )
  }
}

class HorizontalLinearStepper extends React.Component {
  state = {
    activeStep: 0,
    hasFiles: '',
    skipped: new Set(),
    loading: false
  };

  isStepOptional = step => step === this.props.steps.length + 1;

  handleNext = () => {
    const { activeStep } = this.state
    let { skipped } = this.state
    if (this.isStepSkipped(activeStep)) {
      skipped = new Set(skipped.values())
      skipped.delete(activeStep)
    }
    this.setState({
      activeStep: activeStep + 1,
      skipped
    })
  };

  handleBack = () => {
    this.setState(state => ({
      activeStep: state.activeStep - 1
    }))
  };

  handleSkip = () => {
    const { activeStep } = this.state
    if (!this.isStepOptional(activeStep)) {
      // You probably want to guard against something like this,
      // it should never occur unless someone's actively trying to break something.
      throw new Error('You can\'t skip a step that isn\'t optional.')
    }

    this.setState(state => {
      const skipped = new Set(state.skipped.values())
      skipped.add(activeStep)
      return {
        activeStep: state.activeStep + 1,
        skipped
      }
    })
  };

  handleReset = () => {
    this.setState({
      activeStep: 0,
      hasFiles: ''
    })
  };

  isStepSkipped(step) {
    return this.state.skipped.has(step)
  }

  uploadImage = event => {
    event.preventDefault()

    this.handleReset()

    let data = new FormData(event.target)
    let APIURL = '/api/user/completeExperience'
    data.append('experience', this.props.experience._id)
    data.append('resultImage', 'upload:image')

    this.setState({ loading: true })

    fetch(APIURL, {
      method: 'POST',
      body: data
    })
      .then(response => response.json())
      .then(data => {
        this.setState({ loading: false })
        setTimeout(() => {
          this.props.handleClose()
          this.props.updateResult()
        }, 600)
      })
      .catch(e => {
        console.log('error', e)
        this.setState({ loading: false })
      })
  };

  render() {
    const { classes, steps, experience, open } = this.props
    const { activeStep } = this.state

    return (
      <div className={classes.root}>
        <Grid container>
          <Grid item xs>
            <Stepper activeStep={open ? activeStep : -1} orientation="vertical">
              <Step key={'welcome-step'}>
                <StepContent className={classes.stepContent}>
                  <PosedText initialPose="exit" pose="enter">
                    <Typography variant="h3" gutterBottom>
                      <SplitText charPoses={charPoses}>
                        Olá!
                      </SplitText>
                    </Typography>
                    <Typography variant="h5">
                      <SplitText
                        charPoses={charPoses}
                      >{`Vais agora começar a fazer ${
                          experience.name
                        }, quando estiveres pronto carrega no botão Avançar.`}</SplitText>
                    </Typography>
                  </PosedText>
                </StepContent>
                <StepLabel>
                  <Typography variant="body1" component="p" gutterBottom>
                    {'Bem Vindo'}
                  </Typography>
                </StepLabel>
              </Step>
              {steps.map((step, index) => {
                const props = {}
                const labelProps = {}
                if (this.isStepOptional(index)) {
                  labelProps.optional = (
                    <Typography variant="caption">Optional</Typography>
                  )
                }
                if (this.isStepSkipped(index)) {
                  props.completed = false
                }
                return (
                  <Step key={index} {...props}>
                    <StepContent className={classes.stepContent}>
                      <Grid container>
                        {step.image && (
                          <Grid item xs={12} md={4}>
                            <Paper elevation={1}>
                              <img
                                src={step.image.url}
                                alt={`Image ${index}`}
                                style={{ width: '100%' }}
                                className={'rounded'}
                              />
                            </Paper>
                          </Grid>
                        )}
                        <Grid item xs={12} md={step.image ? 8 : 12}>
                          {step.instructions.map((instruction, index) => (
                            <div key={index}>
                              <Typography
                                variant="body1"
                                component="p"
                                className={classes.instruction}
                              >
                                {instruction}
                              </Typography>
                              <Divider />
                            </div>
                          ))}
                        </Grid>
                      </Grid>
                    </StepContent>
                    <StepLabel {...labelProps}>
                      <Typography variant="body1" component="p" gutterBottom>
                        {step.name}
                      </Typography>
                    </StepLabel>
                  </Step>
                )
              })}
              <Step
                key={'last-step'}
                completed={this.isStepSkipped(steps.length + 1)}
              >
                <StepContent className={classes.stepContent}>
                  <PosedText initialPose="exit" pose="enter">
                    <Typography variant="h3" gutterBottom>
                      <SplitText charPoses={charPoses}>{'Parabéns!'}</SplitText>
                    </Typography>
                    <Typography variant="h5">
                      <SplitText
                        charPoses={charPoses}
                      >{`Concluíste a experiência ${
                          experience.name
                        }, agora podes partilhar o teu resultado.`}</SplitText>
                    </Typography>

                    <form onSubmit={this.uploadImage}>
                      <Grid
                        container
                        direction="column"
                        spacing={0}
                        justify="flex-start"
                      >
                        <Grid item xs>
                          <input
                            id="myInput"
                            type="file"
                            accept="image/*"
                            name="image"
                            ref={ref => (this.upload = ref)}
                            onChange={e => {
                              const { target } = e
                              if (target.files.length > 0) {
                                this.setState({
                                  hasFiles: target.files[0].name
                                })
                              } else {
                                this.setState({ hasFiles: '' })
                              }
                            }}
                            style={{ display: 'none' }}
                          />
                          <Button
                            variant="contained"
                            color="default"
                            className={classes.button}
                            onClick={e => this.upload.click()}
                          >
                            Escolhe uma Imagem{' '}
                            <AddIcon className={classes.rightIcon} />
                          </Button>
                        </Grid>
                        <Grid item xs>
                          {this.state.loading ? (
                            <LinearProgress className={classes.fabProgress} />
                          ) : (
                            <Button
                              disabled={this.state.hasFiles === ''}
                              className={`${classes.button}`}
                              variant="contained"
                              color="primary"
                              type="submit"
                              name="submit"
                            >
                                Carregar {this.state.hasFiles}{' '}
                              <CloudUploadIcon className={classes.rightIcon} />
                            </Button>
                          )}
                        </Grid>
                      </Grid>
                    </form>
                  </PosedText>
                </StepContent>
                <StepLabel
                  optional={
                    this.isStepSkipped(steps.length + 1) ? (
                      <Typography variant="caption">Optional</Typography>
                    ) : (
                      ''
                    )
                  }
                >
                  <Typography variant="body1" component="p" gutterBottom>
                    {'Parabéns'}
                  </Typography>
                </StepLabel>
              </Step>
            </Stepper>
          </Grid>
          <Grid
            item
            xs={2}
            style={{
              marginTop: activeStep * 5 + '%',
              transition: 'margin-top .5s ease'
            }}
          >
            <img src={open ? this.props.gif : ''} width="100%" />
          </Grid>
        </Grid>
        <div style={{ marginBottom: '2em' }}>
          {activeStep === steps.length + 1 ? (
            <div>
              <Typography className={classes.instructions}>
                Todos os passos completados - já acabaste
              </Typography>
              <Button
                onClick={this.props.handleClose}
                className={classes.button}
              >
                Concluir
              </Button>
            </div>
          ) : (
            <div>
              <Button
                disabled={activeStep === 0}
                onClick={this.handleBack}
                className={classes.button}
              >
                  Retroceder
              </Button>
              {this.isStepOptional(activeStep) && (
                <Button
                  variant="contained"
                  color="primary"
                  onClick={this.handleSkip}
                  className={classes.button}
                >
                    Saltar
                </Button>
              )}
              <Button
                variant="contained"
                color="primary"
                onClick={this.handleNext}
                className={classes.button}
              >
                {activeStep === steps.length + 1 ? 'Terminar' : 'Avançar'}
              </Button>
            </div>
          )}
        </div>
      </div>
    )
  }
}

export default withStyles(styles)(withMobileDialog()(ExperienceSteps))
