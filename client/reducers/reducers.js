import { combineReducers } from 'redux'
import { GET_DATA, LOADING_DATA } from '../actions/actions'
import { ADD_USER } from '../actions/userActions'
import { ADD_CATEGORIES } from '../actions/categoriesActions'


function getData (state = {}, action) {
  switch (action.type) {
  case GET_DATA:
    return action.payload
  }
  return state
}

function loadingData (state = true, action) {
  switch (action.type) {
  case LOADING_DATA:
    return action.payload
  }
  return state
}

function addUser (state = null, action) {
  switch (action.type) {
  case ADD_USER:
    return action.payload
  }
  return state
}

function addCategories (state = [], action) {
  switch (action.type) {
  case ADD_CATEGORIES:
    return action.payload
  }
  return state
}

const reducers = combineReducers({
  getData: getData,
  loadingData: loadingData,
  addUser: addUser,
  addCategories: addCategories,
})

export default reducers
