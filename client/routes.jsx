import React from 'react'
import { Route, HashRouter, Switch } from 'react-router-dom'
import ScrollToTop from './components/ScrollTop'
import Main from './pages/Main'
import Forum from './pages/Forum'
import Contact from './pages/Contact'
import Signup from './pages/Signup'
import Login from './pages/Login'
import Profile from './pages/Profile'
import Group from './pages/Group'
import SideBar from './components/SideBar'
import ExperiencesPage from './pages/ExperiencesPage'
import ExperiencePage from './pages/ExperiencePage'
import ThreadPage from './pages/Thread'

const Routes = props => (
  <HashRouter>
    <ScrollToTop>
      <div style={{ display: 'flex' }}>
        <SideBar />
        <Switch>
          <Route exact path="/" component={ Main } />
          <Route exact path="/forum" component={ Forum } />
          <Route exact path="/thread/:id" component={ ThreadPage } />
          <Route exact path="/contact" component={ Contact } />
          <Route exact path="/signup" component={ Signup } />
          <Route exact path="/login" component={ Login } />
          <Route exact path="/profile" component={Profile} />
          <Route exact path="/group" component={ Group } />
          <Route exact path="/experiences" component={ ExperiencesPage } />
          <Route exact path="/experiences/:id" component={ ExperiencePage } />
        </Switch>
      </div>
    </ScrollToTop>
  </HashRouter>
)

export default Routes
