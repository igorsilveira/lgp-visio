// We will need to import this from redux to create our store and make use of the thunk
import { createStore, applyMiddleware } from 'redux'
// Dont forget to import redux thunk
import thunk from 'redux-thunk'
// Getting our combined reducers
import reducers from './reducers/reducers'

// Define our store
const store = createStore(reducers, applyMiddleware(thunk))

export default store
