import axios from 'axios'
import store from '../store'

// Exporting our actions
export const ADD_CATEGORIES = 'ADD_CATEGORIES'

// This will get the data from the API
export function addCategories (categories) {
  return {
    type: ADD_CATEGORIES,
    payload: categories,
  }
}

export function fetchCategories () {
  const API_URL = '/api/category'
  axios.get(API_URL)
    .then(response => response.data)
    .then(data => {
      let categories = data.category
      data.category.map((category, index) => {
        categories[category._id] = category
      })
      store.dispatch(addCategories(categories))
    })
    .catch(e => console.log('error', e))
}

fetchCategories()
