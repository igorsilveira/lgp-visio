import axios from 'axios'

// Exporting our actions
export const LOADING_DATA = 'LOADING_DATA'
export const GET_DATA = 'GET_DATA'

// An action to check if the data are loaded accepts true or false
export function loadingData (loading) {
  return {
    type: LOADING_DATA,
    payload: loading,
  }
}

// This will get the data from the API
export function getData (data) {
  return {
    type: GET_DATA,
    payload: data,
  }
}

// This is a redux thunk that will fetch our model data
export function experiencesFetchData (url, type) {
  return dispatch => {
    dispatch(loadingData(true))
    const request = axios.get(url)
    request.then(response => {
      dispatch(getData(response.data[type]))
      dispatch(loadingData(false))
    })
  }
}
