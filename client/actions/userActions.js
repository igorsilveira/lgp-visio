import axios from 'axios'
import store from '../store'

// Exporting our actions
export const ADD_USER = 'ADD_USER'

// This will get the data from the API
export function addUser (user) {
  return {
    type: ADD_USER,
    payload: user,
  }
}

// This is a redux thunk that will fetch our model data
export function fetchUser () {
  const API_URL = '/api/user'
  axios.get(API_URL)
    .then(response => response.data)
    .then(data => {
      store.dispatch(addUser(data))
    })
    .catch(e => console.log('error', e))
}

export function signout () {
  const APIURL = '/api/user/signout'
  axios.get(APIURL)
    .then(response => response.json())
    .then(data => {
      if (data === 'success')
      { store.dispatch(addUser(null)) }
    })
    .catch(e => console.log('error', e))
}

fetchUser()
