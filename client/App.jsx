import React, { Component } from 'react'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import Routes from './routes'
import { blue } from '@material-ui/core/colors'

const theme = createMuiTheme({
  palette: {
    secondary: {
      main: blue[900],
    },
    primary: {
      main: '#007E9F',
    },
  },
  typography: {
    useNextVariants: true,
    fontFamily: ['"Open Sans"', 'sans-serif'].join(','),
  },
})

class App extends Component {
  render () {
    return (
      <div>
        <MuiThemeProvider theme={theme}>
          <Routes />
        </MuiThemeProvider>
      </div>
    )
  }
}

export default App
