test('Fake test', () => {
  expect(true).toBeTruthy()
})

import axios from 'axios'
import React from 'react'
import { shallow, mount, render } from 'enzyme'
import App from './App'

describe('<App />', ()=> {
  it('renders one <App /> component', () => {
    const wrapper = shallow(<App />)
    expect(wrapper.find(App).length).toEqual(0)
  })

  it('renders children when passed in', () => {
    const wrapper = shallow((
      <App>
        <div className="unique" />
      </App>
    ))
    expect(wrapper.contains(<div className="unique" />)).toEqual(false)
  })

})