const FooterSideBar = [
  /*{
    label: 'Linguagem',
    key: 'language',
    icon: 'language',
    pathname: '/lang',
  },*/
  {
    label: 'Entrar',
    key: 'login',
    icon: 'login',
    pathname: '/login',
  },
  {
    label: 'Registo',
    key: 'signup',
    icon: 'signup',
    pathname: '/signup',
  },
  {
    label: 'Terminar Sessão',
    key: 'signout',
    icon: 'login',
    pathname: '/signout',
  },
]

export default FooterSideBar
