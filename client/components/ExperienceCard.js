import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Typography from '@material-ui/core/Typography'
import { Link } from 'react-router-dom'
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import TrendingUp from '@material-ui/icons/TrendingUp'
import ViewQuilt from '@material-ui/icons/ViewQuilt'
import extenso from 'extenso'
import Timer from '@material-ui/icons/Timer'

const styles = {
  card: {
    transition: 'all .3s ease-in-out',
    maxHeight: 400,
    margin:'1em',
    '&:hover':{
      transform: 'scale(1.1)' ,
    }
  },
  media: {
    height: 225
  },
  paper: {
    height: 140,
    width: 100
  },
  name: {
    paddingLeft: 5,
    fontWeight: '800',
    color: '#007E9F',
  },
  icon: {
    marginLeft: 'auto'
  },
  actions: {
    display: 'flex',
    padding: '7px 4px',
    backgroundColor: 'transparent',
    justifyContent: 'center'
  },
  a:{
    '&:hover': {
      textDecoration: 'none !important'
    }, 
  },
  stats: {
    background: 'transparent'
  },
  statsTitle: {
    fontWeight: 'lighter',
    color: '#AAAAAA'
  },
  statsBody: {
    fontWeight: '800',
    color: '#AAAAAA'
  },
  statsIcon: {
    fontWeight: '800',
    color: '#AAAAAA'
  },
  description: {
    fontWeight: 'lighter'
  }
}


function MediaCard(props) {
  const { description, name, _id } = props.experience
  const { experience } = props
  const { classes } = props
  let url

  if (!props.experience.image)
    url =
      'https://www.diocesecpa.org/blog/wp-content/uploads/2018/12/lab-test-experiment-ss-1920.jpg'
  else url = props.experience.image.url

  let cardContent = ''
  if (description) {
    cardContent = <CardContent />
  }
  return (
    <Card className={classes.card} elevation={3}>
      <Link key={_id} to={`/experiences/${_id}`} className={classes.a}>
        <CardActionArea className={classes.ac}>
          <CardMedia className={classes.media} image={url} />
        </CardActionArea>
        <CardContent>
          <Paper className={classes.stats} elevation={0}>
            <Grid item xs container direction="row">
              <Grid
                item
                xs
                container
                direction="column"
                justify="center"
                alignItems="center"
                alignContent="center"
              >
                <Grid item xs>
                  <TrendingUp className={classes.statsIcon} />
                </Grid>
                <Grid item xs>
                  <Typography
                    variant="h6"
                    className={classes.statsTitle}
                    align="center"
                  >
                    Dificuldade
                  </Typography>
                  <Typography
                    variant="body2"
                    align="center"
                    className={classes.statsBody}
                    style={{ textTransform: 'capitalize' }}
                  >
                    {experience.difficulty}
                  </Typography>
                </Grid>
              </Grid>
              <Grid
                item
                xs
                container
                direction="column"
                justify="center"
                alignItems="center"
                alignContent="center"
              >
                <Grid item xs>
                  <ViewQuilt className={classes.statsIcon} />
                </Grid>
                <Grid item xs>
                  <Typography
                    variant="h6"
                    className={classes.statsTitle}
                    align="center"
                  >
                    Passos
                  </Typography>
                  <Typography
                    variant="body2"
                    align="center"
                    className={classes.statsBody}
                    style={{ textTransform: 'capitalize' }}
                  >
                    {extenso(experience.steps.length, { locale: 'pt' })}
                  </Typography>
                </Grid>
              </Grid>
              <Grid
                item
                xs
                container
                direction="column"
                justify="center"
                alignItems="center"
                alignContent="center"
              >
                <Grid item xs>
                  <Timer className={classes.statsIcon} />
                </Grid>
                <Grid item xs>
                  <Typography
                    variant="h6"
                    className={classes.statsTitle}
                    align="center"
                  >
                    Duração
                  </Typography>
                  <Typography
                    variant="body2"
                    className={classes.statsBody}
                    align="center"
                  >
                    {experience.duration
                      ? `~${Math.floor(experience.duration)} Minutos`
                      : 'N/A'}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          </Paper>
        </CardContent>
        <CardActions className={classes.actions}>
          <Typography
            className={classes.name}
            gutterBottom
            variant="h6"
            component="h2"
          >
            {name}
          </Typography>
        </CardActions>
      </Link>
    </Card>
  )
}

MediaCard.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(MediaCard)
