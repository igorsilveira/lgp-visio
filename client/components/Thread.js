import React from 'react'
import Paper from '@material-ui/core/Paper'
import { withStyles } from '@material-ui/core/styles'
import { Avatar, Button, Icon } from '@material-ui/core'
import { unstable_Box as Box } from '@material-ui/core/Box'

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    padding: theme.spacing.unit,
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
  },
  avatarBorder: {
    border: 'solid 2px',
    borderRadius: '50%',
  },
})

class ThreadComponent extends React.Component {
  render () {
    const { classes, thread, categories } = this.props

    return (
      <Paper className={classes.root} elevation={1}>
        <Box display="flex" alignItems="center">
          {thread.categories.map((categoryID, index) => {
            return categories[categoryID] ? (
              <Box
                key={`${index}-${categoryID}`}
                className={classes.avatarBorder}
                borderColor={categories[categoryID].color}
              >
                <Avatar src={categories[categoryID].icon.url}>
                  {categories[categoryID].name.charAt(0)}
                </Avatar>
              </Box>
            ) : null
          })}
          <Box
            mx={1}
            px={1}
            borderLeft={1}
            borderRight={1}
            flexGrow={1}
            borderColor="#dee2e6"
          >
            {thread.name}
          </Box>
          <Button href={`/#/thread/${thread._id}`}>Ler Mais
            <Icon>arrow_forward_ios</Icon>
          </Button>
        </Box>
      </Paper>
    )
  }
}

export default withStyles(styles)(ThreadComponent)
