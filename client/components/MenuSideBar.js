const MenuSideBar = [
  {
    label: 'Página Inicial',
    key: 'home',
    icon: 'home',
    pathname: '/',
  },
  {
    label: 'Perfil',
    key: 'profile',
    icon: 'person',
    pathname: '/profile',
  },
  {
    label: 'Experiências',
    key: 'experiences',
    icon: 'school',
    pathname: '/experiences',
  },
  {
    label: 'Fórum',
    key: 'forum',
    icon: 'forum',
    pathname: '/forum',
  },
  {
    label: 'Grupos',
    key: 'group',
    icon: 'group',
    pathname: '/group',
  },
  /*{
    label: 'Contactos',
    key: 'contact',
    icon: 'contact',
    pathname: '/contact',
  },*/
]

export default MenuSideBar
