import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles'
import { withRouter } from 'react-router-dom'
import { Chip, Avatar } from '@material-ui/core'
import Typography from '@material-ui/core/Typography'
import { Grid } from '@material-ui/core'
import Experiences from '../containers/Experiences'
import Divider from '@material-ui/core/Divider'
import { unstable_Box as Box } from '@material-ui/core/Box'
import { Animated } from 'react-animated-css'

class MyExperimentsTable extends Component {
  constructor(props) {
    super(props)
    this.state = {
      categories: null,
      experiments: [],
      showExperiments: [],
      currentCategories: [],
      userID: null
    }
  }

  // get the category that was selected and actually filter the experiments according to that category
  handleClick(event) {
    event.preventDefault()
    const category = event.currentTarget.innerText
    if (category === 'Tudo') {
      let names = this.props.categories.map(category => category.name)

      this.setState({
        currentCategories: names
      })
    } else if (!this.state.currentCategories.includes(category)) {
      this.setState({
        currentCategories: [...this.state.currentCategories, category]
      })
    } else {
      let newState = this.state.currentCategories.filter(
        currentCategory => currentCategory !== category
      )
      this.setState({
        currentCategories: newState
      })
    }
    this.createFilters()
  }

  createFilters() {
    const { classes } = this.props
    let filters = []
    let label = window.innerWidth > 768 ? true : false
    let counter = 0
    for (const category of this.props.categories) {
      filters.push(
        <Animated key={`${category.name}-filter`} animationIn="bounceInRight" animationInDelay={counter * 100} isVisible={true} >
          <Chip
            avatar={
              <Avatar src={category.icon.url}>{category.name.charAt(0)}</Avatar>
            }
            label={category.name}
            onClick={this.handleClick.bind(this)}
            className={classes.chip}
            data-id={category._id}
            color={
              this.state.currentCategories.includes(category.name)
                ? 'primary'
                : 'default'
            }
          />
        </Animated>
      )
      counter++
    }
    return filters
  }

  render() {
    const { classes, url } = this.props
    const { currentCategories } = this.state

    return (
      <React.Fragment>
        <Grid item xs={12} className={classes.header}>
          <div className={classes.chips}>
            <Chip
              className={classes.chip}
              onClick={this.handleClick.bind(this)}
              data-id="all"
              label="Tudo"
              color={
                this.state.currentCategories.includes('all')
                  ? 'primary'
                  : 'default'
              }
            />

            {this.createFilters()}
          </div>
        </Grid>

        <Box my={2} px={10}>
          <Divider />
        </Box>

        <Experiences url={url} currentCategories={currentCategories} />
      </React.Fragment>
    )
  }
}

const styles = theme => ({
  root: {
    flexGrow: 1,
  },

  header: {
    marginTop: '4em',
    width: '100%',
    display: 'flex',
    paddingBottom: '1em',
    alignItems: 'center',
    justifyContent: 'center'
  },
  chips: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap'
  },

  chip: {
    margin: theme.spacing.unit
  }
})
export default withRouter(withStyles(styles)(MyExperimentsTable))
