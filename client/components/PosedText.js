import posed from 'react-pose'

const PosedText = posed.div({
  exit: {
    x: '-100%',
  },
  enter: {
    x: '0%',
    beforeChildren: false,
    staggerChildren: 50,
  },
})

export default PosedText
