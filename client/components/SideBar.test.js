jest.mock('axios', () => ({
  get: jest.fn((url) => {
    if (url === '/api/user') {
	    	return Promise.resolve({ status: 200, data: {} })
	    }

	    if (url === '/api/category') {
	      return Promise.resolve({ 
	        status: 200, 
	        data: {
	        	category: [
            {
						  	color: '#00A397',
              createdAt: '2019-05-26T21:24:14.804Z',
              icon: {
                format: 'png',
                height: 166,
                public_id: 'visiokids/lbokxfku4bzwgiclw3qm',
                resource_type: 'image',
                secure_url: 'https://res.cloudinary.com/djbrll4ul/image/upload/v1558911386/visiokids/lbokxfku4bzwgiclw3qm.png',
                signature: 'd8469387d5e4f8aee71d02be954e9e84d0be294d',
                url: 'http://res.cloudinary.com/djbrll4ul/image/upload/v1558911386/visiokids/lbokxfku4bzwgiclw3qm.png',
                version: 1558911386,
                width: 167
              },
              key: 'engenharia',
              name: 'Engenharia',
              updatedAt: '2019-05-26T23:07:03.366Z',
              updatedBy: '5ceb03ff24f7502dfc5045a2',
              __v: 0,
              _id: '5ceb03fe24f7502dfc50459f',
						  }
          ]
	        } 
	    	})
	    }

	    return Promise.resolve({ status: 200, data: {} })
  })
}))

import axios from 'axios'
import React from 'react'
import { shallow, mount, render } from 'enzyme'
import { SideBar } from './SideBar'

describe('SideBar', ()=> {
  it('An example of an API call test', async () => {
    // Tests to the api should be done with mocked requests like that
    const categoriesResponse = await axios.get('/api/category')
    expect(categoriesResponse.data.category[0]._id).toBe('5ceb03fe24f7502dfc50459f')

  })

})