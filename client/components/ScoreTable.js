import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles'
import { withRouter } from 'react-router-dom'
import { Avatar, Paper, Grid, Divider } from '@material-ui/core'
import classNames from 'classnames'
import Typography from '@material-ui/core/Typography'
import SchoolIcon from '@material-ui/icons/SchoolOutlined'
import { Animated } from 'react-animated-css'

class ScoreTable extends Component {
  mountTableBody () {
    const { classes, categories, scores } = this.props
    let children = []
    for (let i = 0; i < categories.length; i++) {
      children.push(i === 0 ? null : <Grid item xs={12} key={`${categories[i].name}-divider`} className="mx-2"><Divider /></Grid>)
      children.push(
        <Grid key={`${categories[i].name}-row`} item xs={12} container className="p-2">
          <Grid item xs={3}>
            <Avatar src={categories[i].icon.url} className={classes.bigAvatar}></Avatar>
          </Grid>
          <Grid item xs={5}>
            <div className={classes.headerStyle}>
              {categories[i].name}
            </div>
            <div className={classes.scoreStyle}>
              {scores[categories[i].key].score}
            </div>
          </Grid>
          <Grid item xs={4}>
            <div className={classes.headerStyle}>
                    Experiências:
            </div>
            <div className={classes.scoreStyle}>
              {scores[categories[i].key].count}
            </div>
          </Grid>
        </Grid>        
      )
    }

    return children
  }

  render () {
    const { classes, scores } = this.props
    return (
      <Grid container className={classes.containerStyle} spacing={16}>
        <Grid item xs={12} sm={12} md={12}>
          <Typography variant="h5" className={classes.title}>
            A Minha Pontuação
          </Typography>
        </Grid>

        <Grid item xs={12} sm={8} md={9}>
          <Animated animationIn="bounceInDown" animationInDelay={1000} isVisible={true}>
            <Paper className={classes.scorePaper}>
              <Grid container>
                {this.mountTableBody(classes)}
              </Grid>
            </Paper>
          </Animated>
          
        </Grid>
        
        <Grid item xs={12} sm={4} md={3}>
          <Animated className={classes.totalScore}  animationIn="flipInX" animationInDelay={1400} isVisible={true}>
            <Grid container direction="column" alignItems="center" >
              <Grid item >
                <SchoolIcon className={classes.schoolIcon}/>
              </Grid>
              <Grid item>
                <div className={classes.totalHeaderStyle}>
                      Pontuação Total:
                </div>
                <div className={classes.totalScoreStyle}>
                  {scores.total.score}
                </div>
              </Grid>
              <Grid item>
                <div className={classes.totalHeaderStyle}>
                      Experiências Totais:
                </div>
                <div className={classes.totalScoreStyle}>
                  {scores.total.count}
                </div>
              </Grid>
            </Grid>
          </Animated>
        </Grid>        

        
      </Grid>
    )
  }
}

const styles = theme => ({
  schoolIcon: {
    fontSize: '200px',
    color: '#005578',
  },
  title:{
    fontWeight: 'lighter',
    fontSize: '2em',
  },
  bigAvatar: {
    width: 60,
    height: 60,
  },
  headerStyle: {
    fontWeight: 'bold',
    fontSize: '17px',
  },
  scoreStyle: {
    fontWeight: 'lighter',
    fontSize: '17px',
  },

  scorePaper: {
    padding: theme.spacing.unit,
  },

  totalScore:{
    height: '100%',
    textAlign: 'center',
    border: '6px solid #005578',
  },
  totalHeaderStyle: {
    fontWeight: '800',
    fontSize: '20px',
    maxWidth:'none',
    color: '#005578',
  },
  totalScoreStyle: {
    fontWeight: 'lighter',
    fontSize: '20px',
    color: '#005578',
  },


})
export default withRouter(withStyles(styles)(ScoreTable))
