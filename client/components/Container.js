import React from 'react'

export default ({ children }) => (
  <div
    style={{
      maxWidth: '70%',
      margin: '2rem auto',
      padding: '2rem 1.0875rem',
    }}
  >
    {children}
  </div>
)
