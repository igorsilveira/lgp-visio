import React from 'react'
import { Link, withRouter } from 'react-router-dom'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { withStyles } from '@material-ui/core/styles'
import Drawer from '@material-ui/core/Drawer'
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import List from '@material-ui/core/List'
import CssBaseline from '@material-ui/core/CssBaseline'
import Typography from '@material-ui/core/Typography'
import MenuRoutes from './MenuSideBar'
import FooterRoutes from './FooterSideBar'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import HomeIcon from '@material-ui/icons/HomeOutlined'
import PersonIcon from '@material-ui/icons/AccountCircleOutlined'
import ForumIcon from '@material-ui/icons/QuestionAnswerOutlined'
import ContactIcon from '@material-ui/icons/HelpOutlineOutlined'
import SchoolIcon from '@material-ui/icons/SchoolOutlined'
import LangIcon from '@material-ui/icons/LanguageOutlined'
import SignUpIcon from '@material-ui/icons/PersonAddOutlined'
import LoginIcon from '@material-ui/icons/ExitToAppOutlined'
import PeopleIcon from '@material-ui/icons/PeopleOutlined'
import ClickAwayListener from '@material-ui/core/ClickAwayListener'
import Grid from '@material-ui/core/Grid'
import store from '../store'
import { addUser } from '../actions/userActions'
import visLogo from './resources/vis_logo_icon.png'
import visLogoText from './resources/vis_logo_text.png'
import Fade from '@material-ui/core/Fade'
import withWidth, { isWidthUp } from '@material-ui/core/withWidth'
import ExpansionPanel from '@material-ui/core/ExpansionPanel'
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary'
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'
import Paper from '@material-ui/core/Paper'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'
import Icon from '@material-ui/core/Icon'
import Avatar from '@material-ui/core/Avatar'

const drawerWidth = 185

const authPages = ['profile', 'group', 'signout']
const authPagesExclude = ['login', 'signup']


const styles = theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer - 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    alignItems: 'center',
    boxShadow: 'none',
    backgroundColor: '#fafafa',
    color: '#404041',
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
    alignItems: 'center',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    backgroundImage: 'linear-gradient(to bottom right, #005578, #007E9F)',
    overflowX: 'hidden',
    borderRight: 'none',
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    // width: theme.spacing.unit * 7 + 1,
    // [theme.breakpoints.up('sm')]: {
    //  width: theme.spacing.unit * 9 + 1,
    // },
    width: '57px',
    backgroundImage: 'linear-gradient(to bottom right, #005578, #007E9F)',
    borderRight: 'none',
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    padding: '0 0 0 8px',
    ...theme.mixins.toolbar,
    marginBottom: '30px',
  },
  topToolBar: {
    width: '100%',
    justifyContent: 'center',
    position: 'relative',
  },
  topMenuButton: {
    position: 'absolute',
    left: '10px',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
  },
  list: {
    alignItems: 'center',
  },
  icon: {
    color: '#DDDDDD',
    alignItems: 'center',
    paddingBottom: '20px',
  },
  drawerItem: {
    '&:focus': {
      backgroundColor: 'transparent',
    },
    '&:hover $iconColor': {
      color: '#FFFFFF',
      '-webkit-animation': 'swing .6s',
      'animation': 'swing .6s',
    }, '&:hover $listItemText': {
      color: '#FFFFFF',
    },
  },
  iconColor: {
    'color': '#CCCCCC',
    '&:hover': {
      color: '#FFFFFF',
    },
  },
  listItemText: {
    'color': '#CCCCCC',
    'fontWeight': '300',
    'fontSize': '1rem',
    '&:hover': {
      color: '#FFFFFF',
    },
  },
  listItemIcon: {
    marginRight: '3px',
  },
  listItemTextSelected: {
    color: '#FFFFFF',
    fontWeight: '700',
    fontSize: '1rem',
  },
  iconColorSelected: {
    color: '#FFFFFF',
    paddingBottom: '2px',
    background: 'linear-gradient(#FFFFFF,#FFFFFF) bottom no-repeat',
    backgroundSize: '50% 2px',
    borderRadius: '2px',
  },
  footer: {
    position: 'absolute',
    bottom: '25px',
    width: '179px',
  },
  gridText: {
    marginLeft: '10px',
  },
  gridIcon: {
    paddingTop: '5px',
  },
  appBarText: {
    fontWeight: '800',
    fontSize: '1.75rem',
    color: '#4d4d4d',
  },
  appBarIcon: {
    fontSize: '2.5rem',
    color: '#007E9F',
  },
  fabIcon: {
    height: '40px',
    width: '100%',
    backgroundColor: '#005578',
    boxShadow: 'none',
  },
  chevronIcon: {
    color: 'white',
    position: 'absolute',
    top: '12px',
    fontSize: '1em',
    left: '32px',
  },
  visLogoClick: {
    paddingLeft: '6px',
  },
  visLogoText: {
    paddingLeft: '8px',
    transition: theme.transitions.create('125px', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  userMenu: {
    zIndex: theme.zIndex.drawer + 1,
    position: 'absolute',
    right: '15px',
    top: '5px',
    [theme.breakpoints.up('sm')]: {
      right: '50px',
      top: '10px',
    },
  },
  userMenuIcon: {
    color: '#007E9F',
  },
  userPanel: {
    minWidth: '80px',
    maxWidth: '80px',
    transition: 'min-width 150ms ease-in-out, max-width 150ms ease-in-out',
    [theme.breakpoints.up('md')]: {
      minWidth: '200px',
      maxWidth: '200px',
    },
  },
  userPanel_Active: {
    minWidth: '200px',
    maxWidth: '200px',
    transition: 'min-width 150ms ease-in-out, max-width 150ms ease-in-out',
  },
  userPanelSummary: {
    minHeight: '35px',
    maxHeight: '45px',
  },
  userPanelSummary_noActive: {
    minHeight: '35px',
    maxHeight: '45px',
    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing.unit,
    },
  },
  fontPanel: {
    fontSize: '0.9rem',
    fontWeight: '500',
    color: '#4d4d4d',
    paddingTop: '3px',
    paddingLeft: '4px',
    overflow: 'hidden',
  },
  panelDetails: {
    padding: '0 24px 16px 24px',
  },
  panelDetailsText: {
    fontSize: '0.8rem',
    fontWeight: '600',
    overflow: 'hidden',
  },
  panelIcon: {
    position: 'relative',
    top: '6px',
    width: '1.4rem',
    height: '1.4rem',
    color: '#d3d3d3',
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  avatar: {
    width: '24px',
    height: 'auto',
    color: '#007E9F',
  },
})

class SideBar extends React.Component {
  state = {
    open: false,
    menuDrawer: false,
    auth: true,
    panelExpanded: false,
  };

  handleChange = () => {
    let currentState = store.getState()
    if (this.state.user !== currentState.addUser) {
      this.setState({ user: currentState.addUser })
      this.setState({ auth: 'true' })
    }
  }

  componentDidMount () {
    store.subscribe(this.handleChange)
    const API_URL = '/api/user'

    if (!this.state.user) {
      fetchUser.bind(this)(API_URL)
    }
  }

  signout = (event) => {
    this.closePanel()
    const APIURL = '/api/user/signout'
    fetch(APIURL)
      .then(response => response.json())
      .then(data => {
        if (data === 'success') {
          store.dispatch(addUser(null))
        }
      })
      .catch(e => console.log('error', e))
  }

  handleDrawerOpen = () => {
    this.setState({ open: true })
  };

  handleDrawerClose = () => {
    this.setState({ open: false })
  };

  handleDrawer = () => {
    if (this.state.open) { this.setState({ open: false }) }
    else { this.setState({ open: true }) }
  };


  handleClickPopper = event => {
    const { currentTarget } = event
    this.setState(state => ({
      anchorEl: currentTarget,
      openPopper: !state.openPopper,
    }))
  }

  drawerOpen = (event) => {
    this.setState({ menuDrawer: true })
  }

  drawerClose = (event) => {
    this.setState({ menuDrawer: false })
  }

  getIcon = (icon, location) => {
    const { classes } = this.props

    var icons = {
      home: HomeIcon,
      person: PersonIcon,
      school: SchoolIcon,
      forum: ForumIcon,
      contact: ContactIcon,
      language: LangIcon,
      login: LoginIcon,
      signup: SignUpIcon,
      group: PeopleIcon,
    }


    const ReqIcon = icons[icon]

    if (location === 'sidebar') { return (<ReqIcon className={classNames(classes.iconColor)} />) }
    else if (location === 'sidebarSelected') { return (<ReqIcon className={classNames(classes.iconColorSelected)} />) }
    else { return (<ReqIcon className={classNames(classes.appBarIcon)} />) }

  }

  getHeader = (path) => {
    const { classes } = this.props
    var primary_path = path.split('/')
    var object = MenuRoutes.find(o => o.pathname === '/' + primary_path[1])

    if (!object) { object = FooterRoutes.find(o => o.pathname === '/' + primary_path[1]) }

    if ('/' + primary_path[1] == '/thread')
    { object = MenuRoutes.find(o => o.pathname === '/forum') }


    if (object) {
      var icon = this.getIcon(object.icon, 'appbar')
      return (
        <Grid container direction="row" alignItems="center" justify="center">
          <Grid item align-content="center" className={classNames(classes.gridIcon)}>
            {icon}
          </Grid>
          {isWidthUp('sm', this.props.width) ?
            <Grid item className={classNames(classes.gridText)}>
              <Typography className={classNames(classes.appBarText)} variant="h6" color="inherit" noWrap align="center">
                {object.label}
              </Typography>
            </Grid> : null }
        </Grid>

      )
    }
  }

  isSelected = (item, index) => {
    const { classes } = this.props
    var primary_path = this.props.location.pathname.split('/')
    
    if (item.key == 'signout') {
      item.handler = this.signout
      item.pathname = '/'
    }

    if (item.pathname !== '/' + primary_path[1] || item.key === 'signout') {
      return (
        <ListItem
          component={Link}
          to={{ pathname: item.pathname, search: this.props.location.search }}
          button
          key={item.key}
          onClick={item.handler}
          className={classes.drawerItem}
        >
          <ListItemIcon className={classNames(classes.listItemIcon)}>{this.getIcon(item.icon, 'sidebar')}</ListItemIcon>
          <ListItemText primary={<Typography className={classNames(classes.listItemText)}>{item.label}</Typography>} />
        </ListItem>)
    } else {
      return (
        <ListItem
          component={Link}
          to={{ pathname: item.pathname, search: this.props.location.search }}
          button
          key={item.key}
          className={classes.drawerItem}>
          <ListItemIcon className={classNames(classes.listItemIcon)}>{this.getIcon(item.icon, 'sidebarSelected')}</ListItemIcon>
          <ListItemText primary={<Typography className={classNames(classes.listItemTextSelected)}>{item.label}</Typography>} />
        </ListItem>)
    }
  }

  getLogo = () => {
    const { classes } = this.props

    if (this.state.menuDrawer) {
      return (
        <div>
          <img src={visLogo} className={classNames(classes.visLogoClick)} height="40px" />
          <img src={visLogoText} className={classNames(classes.visLogoText)} height="40px" />
        </div>
      )
    }

    if (this.state.open) {
      return (
        <div>
          <img src={visLogo} onClick={this.handleDrawer} className={classNames(classes.visLogoClick)} height="40px" />
          <Fade in={this.state.open} style={{ transformOrigin: '0 0 0' }}{...(this.state.open ? { timeout: 700 } : {})}>
            <img src={visLogoText} onClick={this.handleDrawer} className={classNames(classes.visLogoText)} height="40px" />
          </Fade>
        </div>
      )
    }
    else {
      return (
        <Grid container alignItems="center" justify="flex-start" style={{ height: '40px', position: 'relative' }}>
          <img src={visLogo} onClick={this.handleDrawer} className={classNames(classes.visLogoClick)} height="40px" />
          <Fade in={!this.state.open} style={{ transformOrigin: '0 0 0' }}{...(this.state.open ? { timeout: 700 } : {})}>
            <Icon className={classes.chevronIcon}>chevron_right</Icon>
          </Fade>
        </Grid>
      )
    }
  }

  getUserPages = (pagesArray) => {
    var routes = []

    var i = 0
    if (this.state.user == null) {
      for (i; i < pagesArray.length; i++) {
        if (!authPages.includes(pagesArray[i].key)) { routes.push(pagesArray[i]) }
      }
    } else {
      for (i; i < pagesArray.length; i++) {
        if (!authPagesExclude.includes(pagesArray[i].key)) { routes.push(pagesArray[i]) }
      }
    }

    return routes
  }

  getUserName = (first, last) => {
    var full = first + ' ' + last

    if (first.length > 12) { full = first.substr(0, 9) + '...' }
    else if (full.length > 12) {
      full = first + ' ' + last.charAt(0) + '.'
    }

    return full
  }

  handlePanelClick = (event) => {
    event.preventDefault()
    this.setState({ panelExpanded: !this.state.panelExpanded })
  }

  closePanel = () => {
    this.setState({ panelExpanded: false })
  }

  getAvatar = () => {
    const { classes } = this.props

    if (typeof this.state.user.avatar !== 'undefined') {
      if (this.state.user.avatar.secure_url == '') {
        return (
          <PersonIcon className={classes.avatar} />
        )
      }
      else {
        return (
          <Avatar alt={this.state.user.name.first + '_' + this.state.user.name.last} src={this.state.user.avatar.secure_url} className={classes.avatar} />
        )
      }
    } else {
      return (
        <PersonIcon className={classes.avatar} />
      )
    }
  }

  getUserMenu = () => {
    const { classes } = this.props

    var user = this.state.user
    if (user != null) {

      return (
        <div className={classes.userMenu}>
          <ClickAwayListener onClickAway={this.closePanel}>
            <Paper>
              <ExpansionPanel
                className={this.state.panelExpanded ? classes.userPanel_Active : classes.userPanel}
                expanded={this.state.panelExpanded}>
                <ExpansionPanelSummary
                  expandIcon={<ExpandMoreIcon />}
                  onClick={this.handlePanelClick.bind(this)}
                  className={this.state.panelExpanded ? classes.userPanelSummary : classes.userPanelSummary_noActive}>

                  <div>
                    {this.getAvatar()}
                  </div>

                  <div className={classNames(classes.fontPanel, !this.state.panelExpanded ? classes.sectionDesktop : null)}>
                    {this.getUserName(user.name.first, user.name.last)}
                  </div>

                </ExpansionPanelSummary>
                <ExpansionPanelDetails className={classes.panelDetails}>
                  <Grid container>
                    <Grid item className="mb-3">
                      <Typography className={classes.panelDetailsText}>
                        {user.name.first + ' ' + user.name.last}</Typography>
                      <Typography className={classes.panelDetailsText}>
                        Papel: {user.role.charAt(0).toUpperCase() + user.role.slice(1)}</Typography>
                      <Typography className={classes.panelDetailsText}>
                        {user.email}</Typography>
                    </Grid>
                    <Grid item>
                      <Typography component="span">
                        <br />
                        <Link to="" onClick={this.signout}>
                          Terminar Sessão
                          <LoginIcon className={classes.panelIcon} />
                        </Link>
                      </Typography>
                    </Grid>
                  </Grid>
                </ExpansionPanelDetails>
              </ExpansionPanel>
            </Paper>
          </ClickAwayListener>
        </div>
      )
    }
  }

  render () {
    const { classes } = this.props

    const drawerContent = (
      <div>
        <div className={classes.toolbar}>
          {this.getLogo()}
        </div>
        <List className={classNames(classes.list)}>
          {this.getUserPages(MenuRoutes).map((item, index) => (
            this.isSelected(item, index)
          ))}
        </List>

        <List className={classNames(classes.footer)}>
          {this.getUserPages(FooterRoutes).map((item, index) => (
            this.isSelected(item, index)
          ))}
        </List>
      </div>
    )

    return (
      <div className={classes.root}>
        <CssBaseline />
        <AppBar
          position="fixed"
          className={classNames(classes.appBar)}
        >
          <Toolbar disableGutters={!this.state.open} className={classes.topToolBar}>
            <IconButton onClick={this.drawerOpen} className={classNames([classes.menuButton, classes.sectionMobile, classes.topMenuButton])} color="inherit" aria-label="Menu">
              <MenuIcon />
            </IconButton>
            {this.getHeader(this.props.location.pathname)}

            {this.getUserMenu()}
          </Toolbar>
        </AppBar>

        <SwipeableDrawer
          open={this.state.menuDrawer}
          onClose={this.drawerClose}
          onOpen={this.drawerOpen}
          anchor="left"
          className={classNames([classes.drawer, classes.sectionMobile])}
          classes={{
            paper: classNames(classes.drawerOpen),
          }}
        >
          {drawerContent}
        </SwipeableDrawer>

        <ClickAwayListener onClickAway={this.handleDrawerClose}>
          <Drawer
            variant="permanent"
            className={classNames([classes.drawer, classes.sectionDesktop], {
              [classes.drawerOpen]: this.state.open,
              [classes.drawerClose]: !this.state.open,
            })}
            classes={{
              paper: classNames({
                [classes.drawerOpen]: this.state.open,
                [classes.drawerClose]: !this.state.open,
              }),
            }}
            open={this.state.open}
          >
            {drawerContent}
          </Drawer>
        </ClickAwayListener>
      </div>
    )
  }
}

SideBar.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
}


function fetchUser (API_URL) {
  fetch(API_URL)
    .then(response => response.json())
    .then(data => {
      store.dispatch(addUser(data))
    })
    .catch(e => console.log('error', e))
}

export { SideBar }
export default withRouter(withWidth()(withStyles(styles, { withTheme: true })(SideBar)))
