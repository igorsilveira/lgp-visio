import React from 'react'

import { withRouter } from 'react-router-dom'
import CssBaseline from '@material-ui/core/CssBaseline'
import Experience from '../containers/Experience'

const ExperiencePage = ({ match }) => {
  return (
    <React.Fragment>
      <CssBaseline />
      <div />
      <Experience id={match.params.id} />
    </React.Fragment>
  )
}

export default withRouter(ExperiencePage)
