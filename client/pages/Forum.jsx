import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles'
import CssBaseline from '@material-ui/core/CssBaseline'
import store from '../store'
import ThreadComponent from '../components/Thread'
import { Chip, Avatar } from '@material-ui/core'
import Divider from '@material-ui/core/Divider'
import Pagination from 'material-ui-flat-pagination'
import { unstable_Box as Box } from '@material-ui/core/Box'
import CircularLoading from '../components/CircularLoading'

const styles = theme => ({
  toolbar: theme.mixins.toolbar,
})
class Forum extends Component {
  state = {
    categories: [],
    threads: null,
    currentCategories: ['all'],
    orderActive: null,
    lastURL: '/api/threads?',
  }

  orderQuery = ['-createdAt', 'views', 'createdAt', '-ranking']

  handleChange = () => {
    let currentState = store.getState()
    if (this.state.categories !== currentState.addCategories) {
      this.setState({ categories: currentState.addCategories })
    }
  }

  // Fetch experiences when component is mounted
  componentDidMount () {
    store.subscribe(this.handleChange)

    this.handleChange()
    const API_URL = '/api/threads?'
    if (!this.state.threads) {
      this.fetchThread(API_URL)
    }
  }

  fetchThread (API_URL) {
    fetch(API_URL)
      .then(response => response.json())
      .then(data => {
        this.setState({ threads: data.threads })
      })
      .catch(e => console.log('error', e))
  }

  handlePages (page) {
    let API_URL = this.state.lastURL + '&page=' + page
    if (this.state.orderActive !== null) { API_URL = API_URL + '&sort=' + this.orderQuery[this.state.orderActive] }
    this.fetchThread(API_URL)
  }

  chipClick (event) {
    event.preventDefault()
    let API_URL = '/api/threads?'
    const categoryID = event.currentTarget.dataset.id
    if (categoryID !== 'all') // eslint-disable-line no-alert
    {
      if (this.state.currentCategories.includes('all')) {
        this.state.currentCategories.splice(this.state.currentCategories.indexOf('all'), 1)
      }

      if (this.state.currentCategories.includes(categoryID)) {
        this.state.currentCategories.splice(this.state.currentCategories.indexOf(categoryID), 1)
      } else this.state.currentCategories.push(categoryID)

      if (this.state.currentCategories.length !== 0) { API_URL = API_URL + 'filter=categories&value=' + this.state.currentCategories }
      else { this.state.currentCategories.push('all') }
    }
    else { this.setState({ currentCategories: ['all'] }) }

    this.setState({ lastURL: API_URL })
    if (this.state.orderActive !== null) { API_URL = API_URL + '&sort=' + this.orderQuery[this.state.orderActive] }
    this.fetchThread(API_URL)
  }

  chipClickOrder (event) {
    // Order By
    event.preventDefault()
    let API_URL = '/api/threads?'
    const orderIndex = event.currentTarget.dataset.orderindex
    if (this.state.orderActive === orderIndex) {
      this.setState({ orderActive: null })
    } else {
      this.setState({ orderActive: orderIndex })
      API_URL = this.state.lastURL + '&sort=' + this.orderQuery[orderIndex]
      this.fetchThread(API_URL)
    }
  }

  render () {
    const { threads, categories } = this.state
    const { classes } = this.props

    if (!threads || categories.length === 0) {
      return (
        <div>
          <CircularLoading />
        </div>
      )
    }

    return (
      <React.Fragment>
        <CssBaseline />
        <Box mt={4} className="container">
          <div className={classes.toolbar} />
          <Box display="flex" flexWrap="wrap" justifyContent="center" alignItems="center" mb={2}>
            Filtros:
            <Box mx={0.5}><Chip onClick={this.chipClick.bind(this)} data-id="all" label="Tudo"
              color={this.state.currentCategories.includes('all') ? 'primary' : 'default'} /></Box>
            {categories ? categories.map((category, index) => {
              return <Box mx={0.5} key={`${category.name}-chip`}><Chip
                avatar={<Avatar src={category.icon.url}>{category.name.charAt(0)}</Avatar>}
                label={category.name}
                data-id={category._id}
                onClick={this.chipClick.bind(this)}
                color={this.state.currentCategories.includes(category._id) ? 'primary' : 'default'}
              /></Box>
            }) : null}
          </Box>
          <Box display="flex" flexWrap="wrap" justifyContent="center" alignItems="center">
            Ordenar por:
            <Box mx={0.5}><Chip label="Mais Recente"
              data-orderindex={0}
              onClick={this.chipClickOrder.bind(this)}
              color={this.state.orderActive === '0' ? 'primary' : 'default'} /></Box>
            <Box mx={0.5}><Chip label="Mais Vistos"
              data-orderindex={1}
              onClick={this.chipClickOrder.bind(this)}
              color={this.state.orderActive === '1' ? 'primary' : 'default'} /></Box>
            <Box mx={0.5}><Chip label="Mais Antigos"
              data-orderindex={2}
              onClick={this.chipClickOrder.bind(this)}
              color={this.state.orderActive === '2' ? 'primary' : 'default'} /></Box>
            <Box mx={0.5}><Chip label="Pontuação"
              data-orderindex={3}
              onClick={this.chipClickOrder.bind(this)}
              color={this.state.orderActive === '3' ? 'primary' : 'default'} /></Box>
          </Box>
          <Box my={2}><Divider /></Box>
          <div>
            {threads
              ? threads.results.map((thread, index) => {
                return (
                  <ThreadComponent
                    key={thread._id}
                    thread={thread}
                    categories={categories}
                  />
                )
              })
              : null}
          </div>
          <Box display="flex" justifyContent="flex-end">
            <Pagination
              limit={1}
              offset={threads ? threads.currentPage - 1 : 0}
              total={threads ? threads.totalPages : 1}
              onClick={(e, offset) => this.handlePages(offset + 1)}
            /></Box>
        </Box>
      </React.Fragment>
    )
  }
}

export default withRouter(withStyles(styles)(Forum))
