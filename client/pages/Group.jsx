import React, { Component } from 'react'
import withStyles from '@material-ui/core/styles/withStyles'
import { withRouter } from 'react-router-dom'
import CssBaseline from '@material-ui/core/CssBaseline'

import classNames from 'classnames'
import Button from '@material-ui/core/Button'
import Card from '@material-ui/core/Card'
import CardMedia from '@material-ui/core/CardMedia'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import DialogContent from '@material-ui/core/DialogContent'
import Dialog from '@material-ui/core/Dialog'
import Slide from '@material-ui/core/Slide'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogActions from '@material-ui/core/DialogActions'
import ArrowForward from '@material-ui/icons/ArrowForwardIos'
import Zoom from '@material-ui/core/Zoom'
import { Divider, LinearProgress } from '@material-ui/core'
import { Animated } from 'react-animated-css'
import { rgbUnit } from 'style-value-types'

function Transition(props) {
  return <Slide direction="up" {...props} />
}

// Styles
const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    flexGrow: 1,
    marginTop: '5em',
    fontFamily: ['"Open Sans"', 'sans-serif'],
    marginBottom: '4em'
  },
  fab: {
    fontWeight: 'bold',
    position: 'fixed',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 2,
    boxShadow: '0px 13px 47px 6px rgba(255,255,255,1)'
  },

  textField: {
    width: '100%'
  },

  iconButton: {
    padding: 10,
    color: '#005578',

    '&:focus': {
      outline: 'none'
    }
  },

  icon: {
    marginRight: theme.spacing.unit * 2
  },

  tablePaper: {
    background: 'none'
  },

  table: {
    border: 'none'
  },

  tableHeadCell: {
    fontWeight: '800'
  },

  tableRowCell: {
    fontWeight: 'lighter'
  },

  heroContent: {
    padding: `${theme.spacing.unit * 4}px 0 ${theme.spacing.unit * 6}px`
  },

  heroButtons: {
    marginTop: theme.spacing.unit * 4
  },
  card: {
    transition: 'all .3s ease-in-out',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    '&:hover':{
      transform: 'scale(1.1)' ,
    }
  },

  cardMedia: {
    paddingTop: '56.25%' // 16:9
  },

  cardContent: {
    flexGrow: 1
  },
  cardGrid: {
    margin: 20,
  },

  groupName: {
    fontWeight:'lighter'
  }
})

class Group extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      open: true,
      groupIDSearched: '',
      groupID: '',
      groupName: '',
      createdAt: '',
      groupPics: [],
      errors: {
        groupIDSearched: false
      }
    }

    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleOpen = () => {
    this.setState({ open: true })
  };

  handleClose = () => {
    this.setState({ open: false })
  };

  render() {
    const { classes, theme } = this.props
    const { open, loading } = this.state

    const transitionDuration = {
      enter: theme.transitions.duration.enteringScreen,
      exit: theme.transitions.duration.leavingScreen
    }

    const disabled = this.state.groupIDSearched.length < 8

    const group = (
      <div className={classes.root}>
        <Grid style={{padding: 30}} container direction="row" spacing={8} justify={'space-between'}>
          <Grid item xs>
            <Typography className={classes.groupName} variant="h3" color={'primary'}>{this.state.groupName}</Typography>
          </Grid>
          <Grid container item xs direction={'column'}>

            <Grid item xs>
              <Typography variant="body1" color={'textSecondary'} align={'right'}>{this.state.createdAt}</Typography>
            </Grid>
            <Grid item xs>
              <Typography variant="body2" color={'textSecondary'} align={'right'}>{this.state.groupID}</Typography>
            </Grid>
          </Grid>
        </Grid>
        <Divider variant={'middle'} light/>
        <div className={classNames(classes.layout, classes.cardGrid)}>
          <Grid container spacing={40}>
            {this.state.groupPics.map((img,index) => (
              <Grid item key={img.url} xs={12} sm={6} md={4} lg={3}>
                <Animated animationIn="bounceInRight" animationInDelay={index*300} isVisible={true}>
                  <a
                    href={img.fullSize}
                    target="_blank">
                    <Card className={classes.card} elevation={1}>
                      <CardMedia
                        className={classes.cardMedia}
                        image={img.url}
                      >
                      </CardMedia>
                    </Card>
                  </a>
                </Animated>
                
              </Grid>
            ))}
          </Grid>
        </div>
      </div>
    )

    return (
      <React.Fragment>
        <CssBaseline />
        {!open && (
          <Zoom
            key={'group-btn'}
            timeout={transitionDuration}
            in={true}
            unmountOnExit
          >
            <Button
              className={classes.fab}
              color="primary"
              variant="contained"
              onClick={this.handleOpen}
            >
                Inserir ID
            </Button>
          </Zoom>
        )}
        {open && (
          <Dialog
            open={open}
            TransitionComponent={Transition}
            onClose={this.handleClose}
            aria-labelledby="form-dialog-title"
          >
            <DialogTitle id="form-dialog-title">
              <Typography variant="h4" color={'primary'} component="p">
                  Identificação de Grupo
              </Typography>
            </DialogTitle>
            <form onSubmit={this.handleSubmit}>
              <DialogContent>
                <DialogContentText>
                    Para poderes ver o conteúdo da visita do teu grupo ao
                    Visionarium, insere o código que te foi dado.
                </DialogContentText>
                <TextField
                  error={this.state.errors.groupIDSearched}
                  style={{ marginTop: 10 }}
                  autoFocus
                  margin="dense"
                  id="id"
                  label={
                    this.state.errors.groupIDSearched
                      ? 'Código Inválido'
                      : 'Código de Identificaçao'
                  }
                  type="text"
                  fullWidth
                  onChange={this.handleChange}
                />
              </DialogContent>
              <DialogActions style={{ padding: 5 }}>
                {loading && <div style={{flexGrow: 1}}><LinearProgress/></div>}
                {!loading && (<div>

                  <Button
                    color="primary"
                    onClick={this.handleClose}
                  >
                      Fechar
                  </Button>
                  <Button
                    style={{marginLeft: 10}}
                    color="primary"
                    variant="contained"
                    disabled={disabled || loading}
                    type="submit"
                  >
                    <div>
                        Continuar{' '}
                      <ArrowForward
                        style={{ marginLeft: 10 }}
                        fontSize="small"
                      />
                    </div>
                  </Button>
                </div>
                )}
              </DialogActions>
            </form>
          </Dialog>
        )}
        {this.state.groupIDSearched.length > 0 && !loading && group}
      </React.Fragment>
    )
  }

  handleSubmit(event) {
    event.preventDefault()

    this.setState({ groupName: '', createdAt: '', groupPics: [], loading: true })
    const APIURL = `/api/group/${this.state.groupIDSearched}`
    fetch(APIURL)
      .then(response => response.json())
      .then(data => {
        let imgs = []
        for (let i = 0; i < data.item.image.length; i++) {
          const imgURL = data.item.image[i].url
          const id = data.item.image[i].public_id
          let url = imgURL.substr(0, imgURL.indexOf('upload/') + 7)
          url += 'c_scale,w_512/' + imgURL.substr(imgURL.indexOf('upload/') + 7, imgURL.indexOf(id) + id.length)
          imgs.push({fullSize: imgURL, url: url})
        }
        let date = data.item.createdAt.split('T')[0]
        
        this.setState({
          groupID: this.state.groupIDSearched,
          groupName: data.item.name,
          createdAt: date,
          groupPics: imgs,
          loading: false,
          open: false
        })

      })
      .catch(e => {
        console.log('error', e)
        this.setState({ errors: { groupIDSearched: true }, loading: false })
      })
  }

  handleChange = event => {
    this.setState({ groupIDSearched: event.target.value })
  };
}

export default withRouter(withStyles(styles, { withTheme: true })(Group))
