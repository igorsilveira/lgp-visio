import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import CssBaseline from '@material-ui/core/CssBaseline'
import { FacebookLoginButton, GoogleLoginButton } from 'react-social-login-buttons'
import classNames from 'classnames'
import { withStyles, withTheme } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import visLogoCor from '../components/resources/vis_logo_cor.png'
import Snackbar from '@material-ui/core/Snackbar'
import SnackbarContent from '@material-ui/core/SnackbarContent'
import IconButton from '@material-ui/core/IconButton'
import CloseIcon from '@material-ui/icons/Close'
import { Animated } from 'react-animated-css'

import store from '../store'
import { addUser } from '../actions/userActions'

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    flexGrow: 1,
    backgroundImage: 'linear-gradient(to bottom right, #005578, #007E9F)',
    minHeight: '100vh',
    paddingLeft: 0,
    paddingRight: 0,
    [theme.breakpoints.up('sm')]: {
      padding: theme.spacing.unit,
      paddingTop: theme.spacing.unit * 5,
    },
  },
  toolbar: theme.mixins.toolbar,
  formContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    [theme.breakpoints.up('sm')]: {
      width: '30em',
    },
  },
  image: {
    width:'100%'
  },
  divtitle: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: '1em',
  },
  title: {
    color: '#fafafa',
    fontWeight: '800',
    fontSize: '3em',
    fontFamily: ['"Open Sans"', 'sans-serif'],
    marginLeft: '.5em',
  },
  loginicon: {
    fontSize: '3em',
    color: '#fafafa',
    marginRight: '.2em',
  },
  formGroup: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: '#fafafa',
    paddingLeft: '3em',
    paddingRight: '3em',
    paddingTop: '2em',
    paddingBottom: '2em',
    boxShadow: '10px 10px 31px -10px rgba(0,0,0,0.5)',
  },
  submitButton: {
    'width': '100%',
    'padding': '.6em',
    'backgroundColor': '#007f9f',
    'borderRadius': '5em',
    'borderColor': 'none',
    'color': '#FFFFFF',
    'marginTop': '1.5em',
    'marginBottom': '1em',
    'fontWeight': '800',
    'fontSize': '1em',

    '&:hover': {
      backgroundColor: '#005578',
    },
    '&:focus': {
      outline: 'none',
    },
  },
  or: {
    textAlign: 'center',
    fontWeight: '800',
    color: '#aaaaaa',
  },
  socialButton: {
    width: '100%',
    borderRadius: '5em',
    textDecoration: 'none',
  },
  snackbar: {
    backgroundColor: '#ffffff',
    boxShadow: ' 0px 1px 1px 0px rgba(0,0,0,0.05),0px 1px 1px 0px rgba(0,0,0,0.14),0px 1px 1px -1px rgba(0,0,0,0.05)',
    color: '#4d4d4d',
  },
  snackbarBase: {
    height: 'auto',
    lineHeight: '28px',
    padding: 24,
    whiteSpace: 'pre-line',
  },
})

class Login extends Component {
  constructor () {
    super()

    this.state = {
      height: window.innerHeight,
      width: window.innerWidth,
      openSnackbar: false,
      snackbarMessage: '',
    }

    this.updateDimensions = this.updateDimensions.bind(this)
  }

  login (event) {
    const API_URL = '/api/user/login'
    event.preventDefault()
    const data = new FormData(event.target)
    fetch(API_URL, {
      method: 'POST',
      body: data,
    }).then(response => response.json())
      .then(reply => {

        if (reply.success)
        { this.props.history.push('/') }
        else if (!reply.success) {
          this.setState({ snackbarMessage: reply.message })
          this.handleSnackbar()
        }

        store.dispatch(addUser(reply.user))

      })
      .catch(e => console.log('error', e))
  }

  updateDimensions () {
    this.setState({ width: window.innerWidth, height: window.innerHeight })
  }

  handleSnackbar = () => {
    this.setState({ openSnackbar: !this.state.openSnackbar })
  }

  getSnackbar = () => {
    const { classes } = this.props

    return (
      <div>
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.openSnackbar}
          autoHideDuration={6000}
          onClose={this.handleSnackbar}
          className={classes.snackbarBase}
        >
          <SnackbarContent
            aria-describedby="client-snackbar"
            message={
              <span id="client-snackbar" className={classes.message}>
                {this.state.snackbarMessage}
              </span>
            }
            className={classes.snackbar}
            action={[
              <IconButton
                key="close"
                aria-label="Close"
                color="inherit"
                onClick={this.handleSnackbar}
              >
                <CloseIcon className={classes.icon} />
              </IconButton>,
            ]}
          />

        </Snackbar>
      </div>
    )
  }

  componentWillMount () {
    this.updateDimensions()
  }
  componentDidMount () {
    window.addEventListener('resize', this.updateDimensions)
  }
  componentWillUnmount () {
    window.removeEventListener('resize', this.updateDimensions)
  }

  render () {
    const { classes } = this.props
    const login = (
      <Grid container alignItems={'center'} direction={'column'} justify={'center'}>
        <Animated animationIn="zoomInUp" animationInDelay={100} isVisible={true}>
          <Grid item  className={classes.formContainer}>
            <form id="sign-up" onSubmit={this.login.bind(this)} className={classes.formGroup}>
              <img className={classes.image} src={visLogoCor} alt="Visionarium Logo" />
              <TextField
                id="email"
                type="email"
                label="E-mail"
                name="email"
                className={classNames('input-box', classes.input, classes.textField)}
                margin="normal"
                inputProps={{ pattern: '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$', title: 'Please use a valid email address.' }}
                required
              />
              <TextField
                id="password"
                type="password"
                label="Palavra-Passe"
                name="password"
                className={classNames('input-box', classes.input, classes.textField)}
                margin="normal"
                required
              />
              <Button type="submit" variant="contained" className={classNames(classes.submitButton)}> Entrar </Button>
              <p className={classes.or}>ou</p>
              <a className={classes.socialButton} href="/api/user/login/facebook"> <FacebookLoginButton text="Entrar com Facebook" style={{ borderRadius: '5em', paddingLeft: '1em', fontSize: '1em' }} /></a>
              <a className={classes.socialButton} href="/api/user/login/google" > <GoogleLoginButton text="Entrar com Google" style={{ borderRadius: '5em', paddingLeft: '1em', fontSize: '1em' }} /> </a>
            </form>
            {this.getSnackbar()}
          </Grid>
        </Animated>
      </Grid>
    )

    return (
      <React.Fragment>
        <CssBaseline />
        <section className={classes.root}>
          <div className={classes.toolbar} />
          {login}
        </section>
      </React.Fragment>
    )
  }
}

export default withRouter(withStyles(styles)(Login))
