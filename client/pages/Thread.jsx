import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { withRouter } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles'
import CssBaseline from '@material-ui/core/CssBaseline'
import { Grid, Paper, Avatar, Divider, TextField, FormControl, Button } from '@material-ui/core'
import Fab from '@material-ui/core/Fab'
import Typography from '@material-ui/core/Typography'
import TrendingUp from '@material-ui/icons/TrendingUp'
import ArrowDropUp from '@material-ui/icons/ArrowDropUp'
import ArrowDropDown from '@material-ui/icons/ArrowDropDown'
import Timer from '@material-ui/icons/Timer'
import MuiExpansionPanel from '@material-ui/core/ExpansionPanel'
import MuiExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'
import store from '../store'
var moment = require('moment')

const ExpansionPanel = withStyles({
  root: {
    'border': 'none',
    'boxShadow': 'none',
    '&:not(:last-child)': {
      borderBottom: 0,
    },
    '&:before': {
      display: 'none',
    },
  },
  expanded: {
    margin: 'auto',
  },
})(MuiExpansionPanel)

const ExpansionPanelDetails = withStyles({
  root: {
    'background-color': '#f4f4f4',
    'padding': 0,
  },
})(props => <MuiExpansionPanelDetails {...props} />)

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    flexGrow: 1,
    padding: theme.spacing.unit,
    marginTop: theme.spacing.unit * 5,
  },
  toolbar: theme.mixins.toolbar,
  avatarBorder: {
    border: 'solid 2px',
  },
  treadContainer: {
    padding: theme.spacing.unit * 3,
    marginBottom: theme.spacing.unit * 3,
  },
  commentsPaper: {
    padding: theme.spacing.unit * 2,
  },
  commentsContainer: {
    padding: theme.spacing.unit * 2,
    backgroundColor: '#f4f4f4',
  },
  commentForm: {
    padding: 0,
    width: '100%',
  },
  commentAuthor: {
    fontWeight: 600,
    color: 'rgba(0, 0, 0, 0.8)',
    textDecoration: 'none',
  },
  comments: {
    padding: '0.75em 0.5em',
  },
  voteButtons: {
    width: 'auto',
    height: 'auto',
    boxShadow: 'none',
    minHeight: 'auto',
    minWidth: 'auto',
    backgroundColor: 'transparent',
  },
})

class ThreadPage extends Component {
  state = {
    threadId: null,
    thread: null,
    categories: [],
    loadedComments: [],
    comments: null,
    showComments: false,
    loadComments: true,
  }

  handleChange = () => {
    let currentState = store.getState()
    if (this.state.categories !== currentState.addCategories) {
      this.setState({ categories: currentState.addCategories })
    }
  }

  componentDidMount() {
    const id = this.props.match.params.id
    this.setState({ threadId: id })
    const API_URL = '/api/threads/' + id
    this.fetchTread(API_URL)
    this.fetchComments(API_URL + '/comments')

    store.subscribe(this.handleChange)
    this.handleChange()
  }

  fetchTread(API_URL) {
    fetch(API_URL)
      .then(response => response.json())
      .then(data => {
        this.setState({ thread: data.thread })
      })
      .catch(e => console.log('error', e))
  }

  handleOnClick(event) {
    event.preventDefault()
    this.setState({ showComments: !this.state.showComments })
  }

  handleMoreOnClick(event) {
    event.preventDefault()
    const threadId = this.state.thread._id
    const page = '?page=' + this.state.comments.next
    const API_URL = '/api/threads/' + threadId + '/comments' + page
    this.fetchComments(API_URL)
  }

  fetchComments(API_URL) {
    fetch(API_URL)
      .then(response => response.json())
      .then(data => {
        let comments = this.state.loadedComments
        let _comments = comments.concat(data.comments.results)
        _comments.sort(compare)
        this.setState({ comments: data.comments, loadedComments: _comments })
        !data.comments.next ? this.setState({ loadComments: false }) : null
      })
      .catch(e => console.log('error', e))
  }

  submitCommit(event) {
    const API_URL = 'api/threads/' + this.state.thread._id + '/comment'
    event.preventDefault()
    const data = new FormData(event.target)
    fetch(API_URL, {
      method: 'POST',
      body: data,
    }).then(response => response.json())
      .then(reply => {
        let comments = this.state.loadedComments
        comments.push(reply.comment)
        this.setState({ loadedComments: comments })
      })
      .catch(e => console.log('error', e))
    event.target.value.value = ''
  }

  vote(commentId, vote) {
    const API_URL = 'api/comments/' + commentId
    var formData = new FormData()
    formData.append('vote', vote)

    fetch(API_URL, {
      method: 'POST',
      body: formData,
      contentType: 'application/x-www-form-urlencoded',
    }).then(response => response.json())
      .then(reply => {
        if (reply.success) {
          const votes = ReactDOM.findDOMNode(this.refs['votes-' + commentId])
          if (vote === '+') { votes.innerHTML = Number.parseInt(votes.innerHTML) + 1 }
          else if (vote === '-') { votes.innerHTML = Number.parseInt(votes.innerHTML) - 1 }
        }
      })
      .catch(e => console.log('error', e))
  }

  upVote(event) {
    event.preventDefault()
    const commentId = event.currentTarget.dataset.id
    this.vote(commentId, '+')
  }

  downVote(event) {
    event.preventDefault()
    const commentId = event.currentTarget.dataset.id
    this.vote(commentId, '-')
  }


  render() {
    const { classes } = this.props
    const { thread, categories, loadedComments } = this.state

    function createCategoryAvatar(id) {
      if (id && categories.length !== 0) {
        return (
          <Avatar
            src={categories[id].icon.url}
            className={classes.avatarBorder}
            style={{ borderColor: categories[id].color }}
          >
            {categories[id].name.charAt(0)}
          </Avatar>
        )
      }
      return <div />
    }

    function getAuthorImage(author) {
      return (
        <Avatar>{author.name.first.charAt(0) + author.name.last.charAt(0)}</Avatar>
      )
    }

    function drawComment(comment) {
      const date = new moment(comment.createdAt)
      let dateDisplay = ''
      if (date.isBefore(moment().startOf('day'))) {
        if (date.isBefore(moment().startOf('year'))) { dateDisplay = date.format('MM/YYYY') }
        dateDisplay = date.format('DD/MM')
      } else dateDisplay = date.fromNow()

      return (
        <Grid key={comment._id} container item xs={12} className={classes.comments}>
          <Grid item className="mr-3">
            {getAuthorImage(comment.author)}
          </Grid>
          <Grid container item xs>
            <Grid container item xs={12} className="mb-1">
              <Typography className={classes.commentAuthor}>
                {comment.author.name.first + ' ' + comment.author.name.last}
              </Typography>
              <Typography
                className="ml-1"
                color="textSecondary">
                {dateDisplay}
              </Typography>
            </Grid>
            <Grid item xs={12}><Typography>{comment.value}</Typography></Grid>
          </Grid>
          <Grid item>
            <Grid container>
              <Grid item xs container direction="column" justify="center">
                <Fab className={classes.voteButtons} data-id={comment._id} onClick={this.upVote.bind(this)}><ArrowDropUp /></Fab>
                <Fab className={classes.voteButtons} data-id={comment._id} onClick={this.downVote.bind(this)}><ArrowDropDown /></Fab>
              </Grid>
              <Grid item xs container alignItems="center">
                <Typography id={`votes-${comment._id}`} ref={`votes-${comment._id}`} >{comment.votes}</Typography>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      )
    }

    function drawRelatedCategories(id) {
      if (id && categories.length !== 0) {
        return (
          <Paper className={classes.treadContainer}>
            <Grid container>
              <Grid container item xs={12} alignItems="center">
                <Grid item >{createCategoryAvatar(id)}</Grid>
                <Grid item>
                  <Typography
                    variant="h5"
                    component="h4"
                    color="textSecondary"
                    className="mb-1 ml-1">
                    {categories[id].name}
                  </Typography></Grid>
              </Grid>
              <Grid item xs={12}><Divider className="my-3" /></Grid>
              <Grid item xs={12}>
              </Grid>
            </Grid>
          </Paper>
        )
      }
    }

    function drawRelatedExperience(experience) {
      if (experience) {
        return (
          <a href={'/#/experiences/' + experience._id} style={{ textDecoration: 'none' }}>
            <Paper className={classes.treadContainer}>
              <Grid container item xs={12} alignItems="center">
                {experience.categories.map((id) => {
                  return <Grid item key={id}>{createCategoryAvatar(id)}</Grid>
                })}
                <Grid item>
                  <Typography
                    variant="h5"
                    component="h4"
                    color="textSecondary"
                    className="mb-1 ml-1">
                    {experience.name}
                  </Typography></Grid>
              </Grid>
              <Grid item xs={12}><Divider className="my-3" /></Grid>
              <Grid item xs container direction="row" spacing={16}>
                <Grid
                  item
                  xs
                  container
                  direction="column"
                  justify="center"
                  alignItems="center"
                  alignContent="center"
                >
                  <Grid item xs>
                    <TrendingUp />
                  </Grid>
                  <Grid item xs>
                    <Typography variant="h6" align="center" style={{ fontSize: '0.875rem' }}>
                      Dificuldade
                    </Typography>
                    <Typography
                      variant="body2"
                      align="center"
                      color="textSecondary"
                      style={{ textTransform: 'capitalize' }}
                    >
                      {experience.difficulty}
                    </Typography>
                  </Grid>
                </Grid>
                <Grid
                  item
                  xs
                  container
                  direction="column"
                  justify="center"
                  alignItems="center"
                  alignContent="center"
                >
                  <Grid item xs>
                    <Timer />
                  </Grid>
                  <Grid item xs>
                    <Typography variant="h6" align="center" style={{ fontSize: '0.875rem' }}>
                      Duração
                    </Typography>
                    <Typography
                      variant="body2"
                      align="center"
                      color="textSecondary">
                      {experience.duration
                        ? `~${Math.floor(experience.duration)} Minutos`
                        : 'N/A'}
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
            </Paper>
          </a>
        )
      }
    }

    return (
      <React.Fragment>
        <CssBaseline />
        <section className={classes.root}>
          <div className={classes.toolbar} />
          {thread
            ? <Grid container spacing={40} >
              <Grid container item lg={8} md={9} xs={12} justify="center">
                <Grid item xs={12}>
                  <Paper className={classes.treadContainer}>
                    <Grid container item xs={12} alignItems="center">
                      {thread.categories.map((id) => {
                        return <Grid item key={id}>{createCategoryAvatar(id)}</Grid>
                      })}
                      <Grid item>
                        <Typography variant="h3" component="h1" className="ml-3 mb-0">
                          {thread.name}
                        </Typography>
                      </Grid>
                    </Grid>
                    <Grid item xs={12}><Divider className="my-3" /></Grid>
                    <Grid item xs={12}>
                      <Grid item xs={12}>
                        <Typography
                          variant="h4"
                          component="h2"
                          color="textSecondary"
                          className="mb-2"
                        >
                          Descrição
                        </Typography>
                      </Grid>
                      <Grid item xs={12}>
                        <div dangerouslySetInnerHTML={{ __html: thread.description.html }} />
                      </Grid>
                      <Grid item xs={12} container justify="center">
                        {thread.image ? <img src={thread.image.url} width="75%" height="75%" /> : null}
                      </Grid>
                      <Grid container item xs={12} className="mt-3" justify="flex-end">
                        <Grid item><Typography
                          color="textSecondary">

                          Criado Por: {thread.author // TODO: Link to user profile
                            ? thread.author.name.first + ' ' + thread.author.name.last : 'anonymous'}
                        </Typography></Grid>
                      </Grid>
                    </Grid>
                  </Paper>
                </Grid>
                <Grid item sm={10} xs={12}>
                  <Paper>
                    <Grid item xs={12} id="comments" className={classes.commentsPaper}>
                      <Grid item xs={12}>
                        <Typography
                          variant="h5"
                          component="h2"
                          color="textSecondary"
                          className="mb-3"
                        >
                          Comentários
                        </Typography>
                      </Grid>
                      <Grid item xs={12}>
                        <FormControl
                          className={classes.commentForm}>
                          <form onSubmit={this.submitCommit.bind(this)}>
                            <TextField
                              className={classes.commentForm}
                              label="Novo Comentário"
                              id="new_comment"
                              name="value"
                            />
                          </form>
                        </FormControl>
                      </Grid>
                    </Grid>
                    <Grid item xs={12} className={classes.commentsContainer}>
                      <Typography
                        color="textSecondary"
                        className="mb-2">
                        {thread.comments.length} Comentários</Typography>
                      <ExpansionPanel
                        square
                        expanded={this.state.showComments}
                        onChange={this.handleOnClick.bind(this)}
                      >
                        <ExpansionPanelDetails>
                          <Grid container>
                            {loadedComments.map((comment) => {
                              return (drawComment.bind(this))(comment)
                            })}
                          </Grid>
                        </ExpansionPanelDetails>
                      </ExpansionPanel>
                      <Grid container item xs={12} justify="center">
                        <Button onClick={this.handleOnClick.bind(this)}><Typography
                          color="textSecondary">
                          {this.state.showComments ? 'Esconder' : 'Ver'} Comentários</Typography>
                        </Button>
                        {this.state.loadComments && this.state.showComments
                          ? <Button onClick={this.handleMoreOnClick.bind(this)}>
                            <Typography
                              color="textSecondary"
                            >
                              Mais Comentários
                            </Typography>
                          </Button>
                          : null}
                      </Grid>
                    </Grid>
                  </Paper>
                </Grid>
              </Grid>
              <Grid container direction="column" item md={3} xs={12}>
                {thread.experiencies.map(experience => {
                  return <Grid item key={experience._id}>{drawRelatedExperience(experience)}</Grid>
                })}
                {thread.categories.map((id) => {
                  return <Grid item key={id}>{drawRelatedCategories(id)}</Grid>
                })}
              </Grid>
            </Grid> : null}
        </section>
      </React.Fragment>
    )
  }
}

function compare(a, b) {
  const date_a = new Date(a.createdAt)
  const date_b = new Date(b.createdAt)
  if (date_a < date_b) {
    return -1
  }
  if (date_a > date_b) {
    return 1
  }
  return 0
}

export default withRouter(withStyles(styles)(ThreadPage))
