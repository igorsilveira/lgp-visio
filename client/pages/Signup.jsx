import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import CssBaseline from '@material-ui/core/CssBaseline'
import classNames from 'classnames'
import { withStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import visLogoCor from '../components/resources/vis_logo_cor.png'
import { Animated } from 'react-animated-css'

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    flexGrow: 1,
    backgroundImage: 'linear-gradient(to bottom right, #005578, #007E9F)',
    minHeight: '100vh',
    paddingLeft: 0,
    paddingRight: 0,
    [theme.breakpoints.up('sm')]: {
      padding: theme.spacing.unit,
      paddingTop: theme.spacing.unit * 5,
    },
  },
  toolbar: theme.mixins.toolbar,
  formContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    [theme.breakpoints.up('sm')]: {
      width: '30em',
    },
  },
  image: {
    width: '100%'
  },
  divtitle: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: '1em',
  },
  title: {
    color: '#fafafa',
    fontWeight: '800',
    fontSize: '3em',
    fontFamily: ['"Open Sans"', 'sans-serif'],
    marginLeft: '.5em',
  },
  loginicon: {
    fontSize: '3em',
    color: '#fafafa',
    marginRight: '.2em',
  },
  formGroup: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: '#fafafa',
    paddingLeft: '3em',
    paddingRight: '3em',
    paddingTop: '2em',
    paddingBottom: '2em',
    boxShadow: '10px 10px 31px -10px rgba(0,0,0,0.5)',
  },
  submitButton: {
    'width': '100%',
    'padding': '.6em',
    'backgroundColor': '#007f9f',
    'borderRadius': '5em',
    'borderColor': 'none',
    'color': '#FFFFFF',
    'marginTop': '1.5em',
    'marginBottom': '1em',
    'fontWeight': '800',
    'fontSize': '1em',

    '&:hover': {
      backgroundColor: '#005578',
    },
    '&:focus': {
      outline: 'none',
    },
  },
})


class Signup extends Component {
  constructor() {
    super()

    this.state = {
      height: window.innerHeight,
      width: window.innerWidth,
    }

    this.updateDimensions = this.updateDimensions.bind(this)
  }

  updateDimensions() {
    this.setState({ width: window.innerWidth, height: window.innerHeight })
  }

  componentWillMount() {
    this.updateDimensions()
  }
  componentDidMount() {
    window.addEventListener('resize', this.updateDimensions)
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions)
  }

  render() {
    const { classes } = this.props

    const signup = (
      <Grid container alignItems={'center'} direction={'column'} justify={'center'} >
        <Animated animationIn="zoomInUp" animationInDelay={100} isVisible={true}>
          <Grid item className={classes.formContainer}>
            <form id="sign-up" className={classes.formGroup} action="/api/user" method="post" >
              <img className={classes.image} src={visLogoCor} alt="Visionarium Logo" />
              <TextField
                id="firstname"
                type="text"
                label="Nome Próprio"
                className={classNames('input-box', classes.input, classes.textField)}
                margin="normal"
                required
                name="name.first"
              />
              <TextField
                id="lastname"
                type="text"
                label="Apelido"
                className={classNames('input-box', classes.input, classes.textField)}
                margin="normal"
                required
                name="name.last"
              />
              <TextField
                id="email"
                type="email"
                label="E-mail"
                className={classNames('input-box', classes.input, classes.textField)}
                margin="normal"
                inputProps={{ pattern: '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$', title: 'Please use a valid email address.' }}
                required
                name="email"
              />
              <TextField
                id="password"
                type="password"
                name="password"
                inputProps={{ pattern: '.{8,}', title: 'Password should contain 8 or more characters.' }}
                label="Palavra-Passe"
                className={classNames('input-box', classes.input, classes.textField)}
                margin="normal"
                required
              />
              <TextField
                id="repeatpassword"
                type="password"
                inputProps={{ name: 'password_check', pattern: '.{8,}', title: 'Password should contain 8 or more characters and match the previous password.' }}
                label="Confirmar Palavra-Passe"
                className={classNames('input-box', classes.input, classes.textField)}
                margin="normal"
                required
              />
              <Button type="submit" variant="contained" className={classNames(classes.submitButton)}> Registar </Button>
            </form>
          </Grid>
        </Animated>
      </Grid>
    )

    return (
      <React.Fragment>
        <CssBaseline />
        <section className={classes.root}>
          <div className={classes.toolbar} />
          {signup}
        </section>
      </React.Fragment>
    )
  }
}

export default withRouter(withStyles(styles)(Signup))
