import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import ScoreTable from '../components/ScoreTable'
import MyExperimentsTable from '../components/MyExperimentsTable'
import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import { Grid, Button } from '@material-ui/core'
import store from '../store'
import CircularLoading from '../components/CircularLoading'
import TextField from '@material-ui/core/TextField'
import CssBaseline from '@material-ui/core/CssBaseline'
import Avatar from '@material-ui/core/Avatar'
import IconButton from '@material-ui/core/IconButton'
import EditIcon from '@material-ui/icons/EditOutlined'
import AddIcon from '@material-ui/icons/AddOutlined'
import { fetchUser } from '../actions/userActions'
import PersonIcon from '@material-ui/icons/AccountCircleOutlined'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import Snackbar from '@material-ui/core/Snackbar'
import SnackbarContent from '@material-ui/core/SnackbarContent'
import CloseIcon from '@material-ui/icons/Close'
import Badge from '@material-ui/core/Badge'
import { Animated } from 'react-animated-css'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItemText from '@material-ui/core/ListItemText'
import Paper from '@material-ui/core/Paper'


const fieldsToEdit
= [
  { label: 'Nome Próprio', type: 'text', field: 'name', field2: 'first' },
  { label: 'Apelido', type: 'text', field: 'name', field2: 'last' },
  { label: 'Palavra-Passe', type: 'password', field: 'password' },
]


class Profile extends Component {
  
  constructor(props) {
    super(props)
    this.state = {
      categories: [],
      user: null,
      topicsImages: null,
      openDialog: false,
      fieldSelected: '',
      textFieldValue: '',
      textFieldValue2: '',
      textFieldValue3: '',
      openSnackbar: false,
      snackbarMessage: '',
    }
  }

  handleChange = () => {
    let currentState = store.getState()
    if (this.state.categories !== currentState.addCategories) {
      this.setState({ categories: currentState.addCategories })
    }
    if (this.state.user !== currentState.addUser) {
      this.setState({ user: currentState.addUser })
    }
  };

  // Fetch experiences when component is mounted
  componentDidMount() {
    store.subscribe(this.handleChange)
    this.handleChange()
  }

  isReady() {
    if (this.state.categories.length <= 0) { return false }
    if (this.state.user == 'undefined' || this.state.user == null) return false
    else return true
  }

  hasExperiments() {
    if (this.state.user.executed == 'undefined' || this.state.user.executed.length <= 0) return false
    else return true
  }
  getAvatar = () => {
    const { classes } = this.props

    if (typeof this.state.user.avatar !== 'undefined') {
      if (this.state.user.avatar.secure_url == '') {
        return (
          <PersonIcon className={classes.avatar} />
        )
      }
      else {
        return (
          <Avatar alt={this.state.user.name.first + '_' + this.state.user.name.last} src={this.state.user.avatar.secure_url} className={classes.avatar} />
        )
      }
    } else {
      return (
        <PersonIcon className={classes.avatar} />
      )
    }
  }

  /*EDIT PROFILE*/

  handleDialogClickOpen = () => {
    this.setState({ openDialog: true })
    this.setState({ textFieldValue: '' })
    this.setState({ textFieldValue2: '' })
    this.setState({ textFieldValue3: '' })
  };

  handleDialogClose = () => {
    this.setState({ openDialog: false })
    this.setState({ textFieldValue: '' })
    this.setState({ textFieldValue2: '' })
    this.setState({ textFieldValue3: '' })
  };

  handleDialogItem = item => e => {
    this.setState({ openDialog: true })
    this.setState({ fieldSelected: item })
  }

  handleTextFieldChange = event => {
    this.setState({ textFieldValue: event.target.value })
  }

  handleTextFieldChange2 = event => {
    this.setState({ textFieldValue2: event.target.value })
  }

  handleTextFieldChange3 = event => {
    this.setState({ textFieldValue3: event.target.value })
  }

  handleSnackbar = () => {
    this.setState({ openSnackbar: !this.state.openSnackbar })
  }


  handleTextFieldSubmit = event => {

    var success = 'false'

    var fieldSelected = 'None'
    var fieldTextValue = 'None'
    var fieldTextValue2 = 'None'
    var fieldTextValue3 = 'None'

    if (this.state.fieldSelected) { fieldSelected = this.state.fieldSelected }

    if (this.state.textFieldValue) { fieldTextValue = this.state.textFieldValue }

    if (this.state.textFieldValue2) { fieldTextValue2 = this.state.textFieldValue2 }

    if (this.state.textFieldValue3) { fieldTextValue3 = this.state.textFieldValue3 }

    if ((fieldSelected === fieldsToEdit[0].label) || (fieldSelected === fieldsToEdit[1].label)) {
      const APIURL = '/api/user/update'

      fetch(APIURL, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ field: fieldSelected, value: fieldTextValue, user: this.state.user }),
      })
        .then(response => response.json())
        .then(data => {
          var userCopy = JSON.parse(JSON.stringify(this.state.user))
          if (data.response === fieldsToEdit[0].label + ' changed') {
            userCopy.name.first = fieldTextValue
            success = true
          } else if (data.response === fieldsToEdit[1].label + ' changed') {
            userCopy.name.last = fieldTextValue
            success = true
          }

          if (success) {
            this.setState({ user: userCopy })
            fetchUser()
            this.setState({ snackbarMessage: data.response })
            this.handleSnackbar()
          }

        })
        .catch(e => console.log('error', e))

    } else if (fieldSelected === fieldsToEdit[2].label) {
      const APIURL = '/api/user/update'

      fetch(APIURL, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(
          {
            field: fieldSelected,
            opassword: fieldTextValue,
            npassword: fieldTextValue2,
            npassword_conf: fieldTextValue3,
            user: this.state.user,
          }
        ),
      })
        .then(response => response.json())
        .then(data => {
          this.setState({ snackbarMessage: data.response })
          this.handleSnackbar()
          if (data.response === 'Password changed.') { success = true }
        })
        .catch(e => console.log('error', e))

    }

    this.handleDialogClose()
  }

  handleAvatarUpload = event => {
    let file = event.target.files[0]


    if (file) {
      var data = new FormData()
      data.append('_id', this.state.user._id)
      data.append('avatar', file)

      var APIURL = '/api/user/updateAvatar'

      fetch(APIURL, {
        method: 'POST',
        body: data,
      })
        .then(response => response.json())
        .then(data => {
          if (data.response === 'Avatar mudado.') {
            this.setState({ snackbarMessage: data.response })
            fetchUser()
          }
          else { this.setState({ snackbarMessage: 'Erro a mudar avatar.' }) }
          this.handleSnackbar()
        })
        .catch(e => console.log('error', e))
    }
  }

  getSnackbar = () => {
    const { classes } = this.props

    return (
      <div>
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.openSnackbar}
          autoHideDuration={6000}
          onClose={this.handleSnackbar}
          className={classes.snackbarBase}
        >
          <SnackbarContent
            aria-describedby="client-snackbar"
            message={
              <span id="client-snackbar" className={classes.message}>
                {this.state.snackbarMessage}
              </span>
            }
            className={classes.snackbar}
            action={[
              <IconButton
                key="close"
                aria-label="Close"
                color="inherit"
                onClick={this.handleSnackbar}
              >
                <CloseIcon className={classes.icon} />
              </IconButton>,
            ]}
          />

        </Snackbar>
      </div>
    )
  }

  getTextField = (field, type) => {

    let labelText = 'Novo ' + field

    if (type === 'text') {
      return (
        <TextField
          autoFocus
          margin="dense"
          id={field}
          label= {labelText}
          type="email"
          inputProps={{
            maxLength: 10,
          }}
          value={this.state.textFieldValue}
          fullWidth
          onChange={this.handleTextFieldChange}
        />)
    } else if (type === 'password') {
      return (
        <div>
          <TextField autoFocus margin="dense" id={field} label="Palavra-Passe Antiga" type="password"
            value={this.state.textFieldValue}
            fullWidth
            onChange={this.handleTextFieldChange}
          />
          <TextField margin="dense" id={field} label="Palavra-Passe Nova" type="password"
            value={this.state.textFieldValue2}
            fullWidth
            onChange={this.handleTextFieldChange2}
          />
          <TextField margin="dense" id={field} label="Confirmação da Nova Palavra-Passe" type="password"
            value={this.state.textFieldValue3}
            fullWidth
            onChange={this.handleTextFieldChange3}
          />
        </div>
      )

    }

  }

  getDialog = () => {
    const { classes } = this.props

    var field = 'Nome Próprio'

    if (this.state.fieldSelected) { field = this.state.fieldSelected }

    var fieldObject = fieldsToEdit.filter(function (o) { return o.label === field })[0]

    return (
      <Dialog
        open={this.state.openDialog}
        onClose={this.handleDialogClose}
        aria-labelledby="form-dialog-title"
        key={`dialog_${field}`}
      >
        <DialogTitle id="form-dialog-title">Editar</DialogTitle>
        <DialogContent className={classes.dialog}>
          {this.getTextField(field, fieldObject.type)}
        </DialogContent>
        <DialogActions>
          <Button onClick={this.handleDialogClose} color="primary">
            Cancelar
          </Button>
          <Button onClick={this.handleTextFieldSubmit} color="primary">
            Atualizar
          </Button>
        </DialogActions>
      </Dialog>
    )
  }


  getFieldValue = (item) => {
    var fieldValue = 'None'

    if (item.field2 !== undefined) { fieldValue = this.state.user[item.field][item.field2] }
    else { fieldValue = this.state.user[item.field] }

    return fieldValue
  }

  editFields = (item, index) => {
    const { classes } = this.props

    var fieldValue = this.getFieldValue(item)

    if (item.type === 'password') { fieldValue = '******' }

    if (item.field2 !== undefined) { fieldValue = this.state.user[item.field][item.field2] }
    else { fieldValue = this.state.user[item.field] }

    if (item.type === 'password') { fieldValue = '******' }


    return (
      <div key={`div_${item.label}`}>
        <ListItem className={classes.listViewItem} >
          <ListItemText
            primary={item.label}
            secondary={fieldValue}
            key={`listitemtext_${item.label}`}
          />


          <ListItemSecondaryAction onClick={this.handleDialogItem(item.label)}>
            <IconButton aria-label="Delete">
              <EditIcon aria-label="Delete" />
            </IconButton>

          </ListItemSecondaryAction>


        </ListItem>

      </div>
    )
  }

  profileEdit = () =>{
    const { classes } = this.props

    return (
      <React.Fragment>
        <CssBaseline />
        
        <Grid container className={classes.profileEdit}  alignItems="stretch" direction="column">
          <Typography variant="h5"  className={classes.title}>
                Editar Perfil
          </Typography>
          <Grid item>
            <Animated animationIn="zoomInDown" animationInDelay={300} isVisible={true}>
              <Grid container className={classes.demo} justify="center">
                <input
                  accept="image/jpeg, image/png"
                  className={classes.input}
                  style={{ display: 'none' }}
                  id="raised-button-file"
                  type="file"
                  onChange={this.handleAvatarUpload}
                />
                <Grid key="profileImage" item >
                  <Badge badgeContent=
                    {
                      <label className={classes.editIcon} htmlFor="raised-button-file">
                        <IconButton aria-label="Add" component="span">
                          <AddIcon />
                        </IconButton>
                      </label>
                    }
                  classes={{ badge: classes.badge }}>

                    {this.getAvatar()}

                  </Badge>
                </Grid>
              </Grid>
            </Animated>
          </Grid>
          <Grid item>
            <List className={classes.list}>
              {fieldsToEdit.map((item, index) => (
                (<Animated key={`${index}-row`} animationIn="bounceInLeft" animationInDelay={300*index+500} isVisible={true}>
                  {this.editFields(item, index)}
                </Animated>)
              ))}
            </List>
          </Grid>
          {this.getDialog()}
          {this.getSnackbar()}
        </Grid>
        
      </React.Fragment>
    )
  }

  render() {
    const { classes } = this.props
    const { user } = this.state
    if (!this.isReady()) {
      return (
        <div>
          <CircularLoading />
        </div>
      )
    }

    return (
      <React.Fragment>
        <CssBaseline />
        <section className={classes.root}>
          <Grid className={classes.mainContainer} direction="row" container spacing={24}>
            <Grid item xs={12} md={3} xl={4} >
              {this.profileEdit()}
            </Grid>
            <Grid item xs={12} md={9} xl={8} >
              <ScoreTable
                categories={this.state.categories}
                scores={JSON.parse(user.score)}
              />
            </Grid>

            <Grid item xs={12} md={12} xl={12} >
              <div>
                <Typography
                  variant="h5"
                  className={classes.title}
                >
                    As Minhas Experiências
                </Typography>

                {this.hasExperiments()
                  ? (
                    <MyExperimentsTable
                      categories={this.state.categories}
                      url={`api/user/${this.state.user._id}/myExperiments`}
                      currentCategories={this.state.currentCategories}
                    />
                  )
                  : (<Grid>
                    <Grid item>
                      <Typography
                        variant="h5"
                        color="textSecondary"
                        className={classes.tableTitle}
                      >
                        Não fizeste nenhuma experiência
                      </Typography>
                    </Grid>
                  </Grid>)
                }
              </div>
            </Grid>

          </Grid>
        </section>
      </React.Fragment>
    )
  }
}

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    flexGrow: 1,
    padding: theme.spacing.unit,
  },

  mainContainer:{
    fontFamily: ['"Open Sans"', 'sans-serif'],
    marginTop: '4em',
  },
  tableTitle: {
    textAlign: 'center',
  },
  title:{
    fontWeight: 'lighter',
    fontSize: '2em',
  },

  /*EDIT PROFILE*/
  profileEdit: {
    width: '100%',
  },
  profileIcon: {
    fontSize: '50px',
    color: '#005578',
  },
  overflow: {
    flexShrink: '0',
  },
  containerStyle: {
    display: 'flex',
    flexShrink: '0',
    flexWrap: 'wrap',
    overflowX: 'hidden',
  },
  profilePicStyle: {
    width: '15em',
    padding: theme.spacing.unit,
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
    display: 'flex',
    alignItems: 'center',
  },
  tableTitle: {
    paddingLeft: '24px',
  },
  listViewItem: {
    backgroundColor: '#ffffff',
    borderRadius: '8px',
    boxShadow: ' 0px 1px 1px 0px rgba(0,0,0,0.05),0px 1px 1px 0px rgba(0,0,0,0.14),0px 1px 1px -1px rgba(0,0,0,0.05)',
    marginBottom: '10px',
  },
  progress: {
    margin: theme.spacing.unit * 2,
  },
  dialog: {
    minWidth: '250px',
    boxShadow: 'none !important',
  },
  close: {
    padding: theme.spacing.unit / 2,
  },
  snackbar: {
    backgroundColor: '#ffffff',
    boxShadow: ' 0px 1px 1px 0px rgba(0,0,0,0.05),0px 1px 1px 0px rgba(0,0,0,0.14),0px 1px 1px -1px rgba(0,0,0,0.05)',
    color: '#4d4d4d',
  },
  snackbarBase: {
    height: 'auto',
    lineHeight: '28px',
    padding: 24,
    whiteSpace: 'pre-line',
  },
  list: {
    paddingRight: '16px',
  },
  avatar: {
    width: '200px',
    height: 'auto',
    color: '#005578',
  },
  badge: {
    top: '80%',
    right: '20px',
    backgroundColor: '#ffffff',
    boxShadow: ' 0px 1px 1px 0px rgba(0,0,0,0.05),0px 1px 1px 0px rgba(0,0,0,0.14),0px 1px 1px -1px rgba(0,0,0,0.05)',
    color: '#4d4d4d',
    height: '50px',
    width: '50px',
    borderRadius: '50%',
  },
  editIcon: {
    paddingTop: '6px',
  },
})

export default withRouter(withStyles(styles)(Profile))
