import React, { Component } from 'react'
import withStyles from '@material-ui/core/styles/withStyles'
import { withRouter } from 'react-router-dom'
import CssBaseline from '@material-ui/core/CssBaseline'
import store from '../store'
import { Grid } from '@material-ui/core'
import classNames from 'classnames'
import ForumIcon from '@material-ui/icons/QuestionAnswerOutlined'
import SchoolIcon from '@material-ui/icons/SchoolOutlined'
import PeopleIcon from '@material-ui/icons/PeopleOutlined'
import homeVideo from './Resources/home_video.mp4'
import { Animated } from 'react-animated-css'


const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    flexGrow: 1,
    minHeight: '100vh',
    fontFamily: ['"Open Sans"', 'sans-serif'],
    padding: theme.spacing.unit,
    [theme.breakpoints.up('sm')]: {
      padding: theme.spacing.unit,
      marginTop: theme.spacing.unit * 5,
    },
  },
  toolbar: theme.mixins.toolbar,

  mainDiv: {
    display: 'flex',
    flexDirection: 'row',
    width: '80%',
    marginBottom: '2em',
    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
      width: '100%',
    },
  },

  steamTitle: {
    fontWeight: 'lighter',
    fontSize: '3em',
    [theme.breakpoints.down('sm')]: {
      fontSize: '2em',
    },
  },

  bold: {
    fontWeight: '800',
  },

  descriptionLinks: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    marginRight: '3em',
    [theme.breakpoints.down('sm')]: {
      marginRight: '0em',
    },

  },

  titleLogoDescription: {
    display: 'flex',
    flexDirection: 'column',
  },

  logoDescription: {
    display: 'flex',
    flexDirection: 'column',
    marginTop: '1.3em',
  },

  description: {
    fontSize: '1.2em',
    fontWeight: 'lighter',
    marginBottom: '1em'
  },

  linksVideo: {
    display: 'flex',
    flexDirection: 'column',
  },
  links: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    color: '#005578',
    flexWrap: 'wrap',
    marginBottom: '1em',
    marginTop: '0.5em',
  },
  video: {
    width: '100%',
    heigth: '100%',
  },

  divLink: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    border: '4px solid #005578',
    padding: '0.5em',
    [theme.breakpoints.down('xl')]: {
      width: '20em',
    },
    [theme.breakpoints.down('lg')]: {
      width: '10em',
    },
    [theme.breakpoints.down('md')]: {
      width: '7em',
    },
    [theme.breakpoints.down('sm')]: {
      width: '12em',
    },
    [theme.breakpoints.down('xs')]: {
      width: '7em',
    },
  },

  iconLink: {
    fontSize: '4em',
  },

  textLink: {
    fontSize: '1.3em',
    fontWeight: '800',
    [theme.breakpoints.down('sm')]: {
      fontSize: '1em',
    },
  },


  visiokids: {
    display: 'flex',
    flexDirection: 'column',
  },

  visiokid: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: '1.3em',
    padding: '1em',
    width: '25em',
    boxShadow: '0px 0px 10px 0px rgba(0,0,0,0.3)',
    borderRadius: '15% / 50%',
    [theme.breakpoints.down('md')]: {
      width: '100%',
    },
    [theme.breakpoints.down('sm')]: {
      borderRadius: '10% / 50%',
    },
  },

  visioTitle: {
    fontWeight: 'lighter',
    fontSize: '3em',
    [theme.breakpoints.down('sm')]: {
      marginTop: '1em',
      fontSize: '2em',
    },
  },

  visiotitles: {
    display: 'flex',
    flexDirection: 'column',
    marginLeft: '1.5em',
    justifyContent: 'center',
  },

  visioname: {
    fontWeight: '800',
    fontSize: '2em',
  },

  visioArea: {
    fontWeight: 'lighter',
    fontSize: '1.5em',
  },

  visioimg: {
    maxWidth: '6.5em',
    maxHeight: '6.5em',
    boxShadow: '0px 0px 52px 0px rgba(0,0,0,0.5)',
    borderRadius: '50%',

    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },

  background_ciencia: {
    backgroundImage: 'linear-gradient(to left, #F5AA3B, #aa711d)',
    color: '#FFFFFF',
  },
  background_tecnologia: {
    backgroundImage: 'linear-gradient(to left, #666666, #333333)',
    color: '#FFFFFF',
  },
  background_engenharia: {
    backgroundImage: 'linear-gradient(to left, #00A397, #004c46)',
    color: '#FFFFFF',
  },
  background_artes: {
    backgroundImage: 'linear-gradient(to left, #0071B6, #014168)',
    color: '#FFFFFF',
  },
  background_matematica: {
    backgroundImage: 'linear-gradient(to left, #DD3333, #911b1b)',
    color: '#FFFFFF',
  },
})

class Main extends Component {
  state = {
    categories: [],
    checked: false,
  };

  handleChange = () => {
    let currentState = store.getState()
    if (this.state.categories !== currentState.addCategories) {
      this.setState({ categories: currentState.addCategories })
    }
  }

  componentDidMount() {
    store.subscribe(this.handleChange)
    this.handleChange()

    this.setState(state => ({ checked: true }))
  }

  render() {
    const { classes } = this.props
    const visioNames = { artes: 'Cassiopeia', ciencia: 'Atómico', matematica: 'Vita', tecnologia: 'Bit', engenharia: 'Cósmico' }

    function drawVisioKids(categoy, index) {
      return (
        <Animated key={visioNames[categoy.key]} animationIn="bounceInRight" animationInDelay={1500 + index * 100} isVisible={true}>
          <div className={classNames(classes.visiokid, classes['background_'+categoy.key])}>
            <Animated animationIn="zoomInRight" animationInDelay={1600 + index * 100} isVisible={true}>
              <img className = {classes.visioimg} src={categoy.icon.url}></img>
            </Animated>
            <div className = {classes.visiotitles}>
              <h3 className = {classes.visioname}>{visioNames[categoy.key]}</h3>
              <h4 className = {classes.visioArea}>{categoy.name}</h4>
            </div>
          </div>
        </Animated>
      )
    }

    return (
      <React.Fragment>
        <CssBaseline />
        <section className={classes.root}>
          <div className={classes.toolbar} />
          <Grid container alignItems={'center'} justify="center" direction={'column'} >
            <div className={classes.mainDiv}>
              <div className={classes.descriptionLinks}>

                <div className={classes.titleLogoDescription}>
                  <h1 className={classes.steamTitle}> <span className={classes.bold}>STEAM</span> @Visionarium</h1>
                  <div className={classes.logoDescription}>
                    <Animated animationIn="fadeInLeftBig" animationInDelay={200} isVisible={true}>
                      <div className={classes.description}>
                    Plataforma interativa direcionada a realização de atividades no âmbito STEAM, focando-se então em temas relacionados com Ciência, Tecnologia, Engenharia, Matemática e, adicionalmente, Arte. Estas atividades são detalhadas no que se é esperado fazer e exploram os temas que abordam de uma forma didática.
                        <br/>
                    Sendo uma plataforma com objetivo educativo, também é possível a avaliação da performance de quem realiza as atividades e a sua discussão a partir de um fórum.
                        <br/>
                    O produto pode ser utilizado tanto no Europarque, onde serão realizadas as atividades do Visionarium, como online, caso seja impossível a deslocação ao local, havendo atividades adaptadas para serem realizadas nos dois casos.
                      </div>
                    </Animated>
                  </div>
                </div>
                <h2 className={classes.visioTitle}>O Site</h2>
                <div className={classes.linksVideo}>
                  <div className={classes.links}>
                    <Animated animationIn="flipInX" animationInDelay={400} isVisible={true} >
                      <div className={classes.divLink}>
                        <Animated animationIn="swing" animationInDelay={700} isVisible={true} >
                          <SchoolIcon className={classes.iconLink}/>
                        </Animated>
                        <h3 className={classes.textLink}>Aprende</h3>
                      </div>
                    </Animated>

                    <Animated animationIn="flipInX" animationInDelay={700} isVisible={true}>
                      <div className={classNames(classes.divLink, classes.secondLink)}>
                        <Animated animationIn="swing" animationInDelay={1000} isVisible={true} >
                          <ForumIcon className={classes.iconLink}/>
                        </Animated>
                        <h3 className={classes.textLink}>Discute</h3>
                      </div>
                    </Animated>


                    <Animated animationIn="flipInX" animationInDelay={1000} isVisible={true}>
                      <div className={classes.divLink}>
                        <Animated animationIn="swing" animationInDelay={1300} isVisible={true} >
                          <PeopleIcon className={classes.iconLink}/>
                        </Animated>
                        <h3 className={classes.textLink}>Relembra</h3>
                      </div>
                    </Animated>
                  </div>
                  <Animated animationIn="zoomIn" animationInDelay={1300} isVisible={true} >
                    <video controls autoPlay={'autoplay'} className={classes.video}>
                      <source src={homeVideo} type="video/mp4" />
                    </video>
                  </Animated>
                </div>
              </div>

              <div className={classes.visiokids}>
                <h2 className={classes.visioTitle}>VisioKids</h2>

                {this.state.categories.map((categoy, index) => {
                  return drawVisioKids(categoy, index)
                })}

              </div>
            </div>
          </Grid>
        </section>
      </React.Fragment>
    )
  }
}

export default withRouter(withStyles(styles)(Main))
