import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import CssBaseline from '@material-ui/core/CssBaseline'
import MyExperimentsTable from '../components/MyExperimentsTable'
import Grid from '@material-ui/core/Grid'
import store from '../store'
import { withStyles } from '@material-ui/core/styles'

class ExperiencesPage extends Component {
  constructor (props) {
    super(props)
    this.state = {
      categories: null,
      currentCategories: ['all']
    }
  }

  handleChange = () => {
    let currentState = store.getState()
    if (this.state.categories !== currentState.addCategories) {
      this.setState({ categories: currentState.addCategories })
    }
  };

  // Fetch experiences when component is mounted
  componentDidMount () {
    store.subscribe(this.handleChange)
    this.handleChange()
  }

  isReady () {
 
    if (this.state.categories === null)
    { return false }
    else return true
  }

  render () {
    const { classes } = this.props
    return (
      <React.Fragment>
        <CssBaseline />

        <Grid container className={classes.root} >
          <Grid item  xs={12} md={12} lg={12} >
            {this.isReady() ? (
              <MyExperimentsTable
                categories={this.state.categories}
                url="/api/experiences"
                currentCategories={this.state.currentCategories}
              />
            ) : null}
          </Grid>
        </Grid>
      
      </React.Fragment>
    )
  }
}

const styles = theme => ({
  root: {
    flexGrow: 1,
    fontFamily: ['"Open Sans"', 'sans-serif'],
    paddingTop: '1em',
    paddingRight : '5em',
    paddingLeft : '5em',
    [theme.breakpoints.down('md')]: {
      paddingRight : '10px',
      paddingLeft : '10px'
    },
  },

})

export default withRouter(withStyles(styles)(ExperiencesPage))
