# lgp-1b

## Set-Up

1) Install MongoDB Client in your envoriment

2) Run de following command:

    ```npm
        npm install
    ```

## Development

1) Open two distinct consoles and run:

    ```
        npm run compile   // This is to watch modifications on React Client
        npm run dev     // This is to start running the server
    ```

2) If you want to check the the Lint, run:

    ```
        npm run lint
    ```