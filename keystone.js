// Simulate config options from your production environment by
// customising the .env file in your project's root folder.
require('dotenv').config()

// Require keystone
var keystone = require('keystone')
var handlebars = require('express-handlebars')

// Initialise Keystone with your project's configuration.
// See http://keystonejs.com/guide/config for available options
// and documentation.

keystone.init({
  'name': 'Visionarium',
  'brand': 'Visionarium',

  'sass': 'server/public',
  'static': 'server/public',
  'favicon': 'server/public/favicon.ico',
  'views': 'server/templates/views',
  'view engine': '.hbs',
  'mongo': process.env.MONGO_URI || 'mongodb://root:example@mongo:27017',
  'cloudinary': process.env.CLOUDINARY_URL,

  'custom engine': handlebars.create({
    layoutsDir: 'server/templates/views/layouts',
    partialsDir: 'server/templates/views/partials',
    defaultLayout: 'default',
    // eslint-disable-next-line no-new-require
    helpers: new require('./server/templates/views/helpers')(),
    extname: '.hbs',
  }).engine,

  'auto update': true,
  'updates': 'server/updates',
  'session': true,
  'auth': true,
  'user model': 'User',
})

// Load your project's Models
keystone.import('server/models')

// Setup common locals for your templates. The following are required for the
// bundled templates and layouts. Any runtime locals (that should be set uniquely
// for each request) should be added to ./routes/middleware.js
keystone.set('locals', {
  _: require('lodash'),
  env: keystone.get('env'),
  utils: keystone.utils,
  editable: keystone.content.editable,
})

// Load your project's Routes
keystone.set('routes', require('./server/routes'))

// Configure the navigation bar in Keystone's Admin UI
keystone.set('nav', {
  enquiries: ['enquiries'],
  users: ['users', 'user-results'],
  experiences: ['experiences', 'experience-steps', 'experience-materials', 'materials', 'categories', 'assets'],
  groups: ['groups'],
  forum: ['threads', 'comments'],
})

keystone.set(
  'adminui custom styles',
  './server/public/styles/keystone/keystone.less'
)
keystone.set('admin path', 'admin')
keystone.set('signin logo', '../images/logoAdmin.png')

// Start Keystone to connect to your database and initialise the web server
keystone.start()
