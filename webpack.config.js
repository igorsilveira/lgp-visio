var path = require('path')

module.exports = {
  // Since webpack 4 we will need to set in what mode webpack is running
  mode: 'production',
  // This will be the entry file for all of our React code
  entry: [
    './client/index.jsx',
  ],
  // This will be where the final bundle file will be outputed
  output: {
    path: path.join(__dirname, '/server/public/react/'),
    filename: 'bundle.js',
    publicPath: '/react/',
  },
  // Adding babel loader to compile our javascript and jsx files
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              '@babel/preset-env',
              '@babel/preset-react',
            ],
            plugins: [
              '@babel/plugin-proposal-class-properties',
            ],
          },
        },
      },
      {
        test: /\.(png|jpg|gif|mp4)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
            },
          },
        ],
      },
    ],
  },
  watch: false,
  resolve: {
    extensions: ['.js', '.jsx', '.scss'],
  },
}
